﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Windows.Controls;

namespace Childes_GUI_3
{




    class EventLogger
    {

        public enum EventType
        {
            Info,
            Event,
            Debug, 
            Warning,
            Error
        }

        bool bLoggingFail = false;
        System.IO.StreamWriter logger = null;

        private static EventLogger _instance = null;

        /// <summary>
        /// Gets the message string based on the event type.
        /// </summary>
        /// <param name="e">The event type.</param>
        /// <returns></returns>
        private String getMessageString(EventType e)
        {

            switch (e)
            {
                case EventType.Info:
                    return "INFO";

                case EventType.Event:
                    return "EVENT";

                case EventType.Debug:
                    return "DEBUG";
                case EventType.Error:
                    return "ERROR";

                case EventType.Warning:
                    return "WARNING";

                default:
                    return "INFO";
            }

            

        }

        /// <summary>
        /// Gets the singleton instance of the EventLogger
        /// </summary>
        /// <returns></returns>
        public static EventLogger instance()
        {
            if (_instance == null)
            {
                _instance = new EventLogger();
            }

            return _instance;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="EventLogger"/> class from being created.
        /// </summary>
        private EventLogger()
        {
            
            String pathName = System.IO.Directory.GetCurrentDirectory() + "/log.txt";





            if (!System.IO.File.Exists(pathName))
            {
                try
                {
                    logger = File.CreateText(pathName);
                }
                catch (UnauthorizedAccessException except)
                {
                System.Windows.MessageBox.Show("Could not create a log file in this location: " + pathName + ". No debugging information will be logged. Exception is " + except.Message + ".",
                        "Failed to log debugging information");


                }
            }
            else
            {
                logger = File.AppendText(pathName);
            }
            String dateString = DateTime.Now.ToString("dd - MM - yyyy HH: mm:ss");



            logger.WriteLine("*********************************");
            logger.WriteLine("Session on " + dateString + ":");

        }

        /// <summary>
        /// Determines whether [logging is valid in this context].
        /// </summary>
        private bool isLoggingValid()
        {

            return (Properties.Settings.Default.bDebugLog && logger != null);

        }
        /// <summary>
        /// Logs the message.
        /// </summary>
        /// <param name="msg">The message content.</param>
        /// <param name="e">The type of event this message belongs to.</param>
        public void LogMsg(String msg, EventType e)
        {
            if ( isLoggingValid())
            {

                // Get the datetime for the message.

                DateTime msgTime = DateTime.Now;

                String msgTimeString = msgTime.ToString("dd-MM-yyyy HH:mm:ss");

                // Append date to message.
                msg = "[" + msgTimeString + "]\t" + getMessageString(e) + ": " + msg;



                

                logger.WriteLine(msg);



            } 
        }

        /// <summary>
        /// Shortcut function to log the value of something.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="value">The value.</param>
        public void ValueLog(String id, String value)
        {

            string output = "The value of {0} is currently {1}";

            LogMsg(String.Format(output, id, value), EventType.Info);

        }

        /// <summary>
        /// Shortcut to log the pressing of a button
        /// </summary>
        /// <param name="id">The identifier of the button.</param>
        public void ButtonClickLog(String id)
        {

            string output = "{0} was pressed.";

            LogMsg(String.Format(output, id), EventType.Event);
           
            
        }

        /// <summary>
        /// Manually writes a string to the log file.
        /// </summary>
        /// <param name="content">The content to write into the .</param>
        public void ManualWrite(string content)
        {
            logger.WriteLine(content);
        }

        /// <summary>
        /// Shortcut to log the pressing of a button
        /// </summary>
        /// <param name="sender">The button that created the click event.</param>
        public void ButtonClickLog(object sender)
        {
            string name = ((Control)sender).Name;

            ButtonClickLog(name);
        }

        /// <summary>
        /// Finishes the logging.
        /// </summary>
        public void finishLogging()
        {
            logger.Flush();
        }
    }
}
