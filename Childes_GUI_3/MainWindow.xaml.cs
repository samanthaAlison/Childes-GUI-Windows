﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using ChildesClassLibrary;
using System.IO;
using System.Windows.Controls.Primitives;
using System.ComponentModel;

namespace Childes_GUI_3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDisposable
    {
        const String BEGINING_OF_FILE = "<Begining of file>";
        const String END_OF_FILE = "<End of file>";
        private int TagLineNumber = 0;

        Color FILE_EDGE_COLOR = Color.FromRgb(255, 255, 255);
        Color LINE_PROCESSED_COLOR = Color.FromRgb(194, 214, 155);
        Color LINE_NOT_PROCESSED_COLOR = Color.FromRgb(217, 149, 148);


        private ChildesFileInterface ChildesFile;
        private ChildesTagLineInterface TagLine;

        public MainWindow()
        {
            InitializeComponent();
            ChildesFile = new ChildesFileInterface();
            lMainGrid.DataContext = ChildesFile;
            
            teLineNumber.DataContext = this;

            Reset();

            

           
        }



        /// <summary>
        /// Sets the tag line and updates the UI.
        /// </summary>
        public void UpdateTagLineView()
        {

            // Log that we are updating the GUI.
            EventLogger logger = EventLogger.instance();



            // Get a 1 starting-index value for the tag line we are on.
            TagLineNumber = ChildesFile.Position + 1;


            logger.LogMsg(String.Format("Updating the GUI for tag line #{0}.", TagLineNumber), EventLogger.EventType.Info);

            this.teLineNumber.Text = TagLineNumber.ToString();

            TagLine = ChildesFile.TagLine;

        

            tbLineProcessed.DataContext = TagLine;



            tvCurrentLine.Text = TagLine.DialogueLine;
            if (TagLine.Processed)
            {
                tvCurrentLine.Background = new SolidColorBrush(LINE_PROCESSED_COLOR);
            }
            else
            {
                tvCurrentLine.Background = new SolidColorBrush(LINE_NOT_PROCESSED_COLOR);
            }

            if (TagLineNumber > 1)
            {
                tvPreviousLine.Text = ChildesFile.GetTagLine(TagLineNumber - 2).DialogueLine;
                if (ChildesFile.GetTagLine(TagLineNumber - 2).Processed)
                {
                    tvPreviousLine.Background = new SolidColorBrush(LINE_PROCESSED_COLOR);
                }
                else
                {
                    tvPreviousLine.Background = new SolidColorBrush(LINE_NOT_PROCESSED_COLOR);
                }
            }
            else
            {
                tvPreviousLine.Text = BEGINING_OF_FILE;
                tvPreviousLine.Background = new SolidColorBrush(FILE_EDGE_COLOR);
            }



            if (TagLineNumber != ChildesFile.TagLineCount)
            {
                tvNextLine.Text = ChildesFile.GetTagLine(TagLineNumber).DialogueLine;
                if (ChildesFile.GetTagLine(TagLineNumber).Processed)
                {
                    tvNextLine.Background = new SolidColorBrush(LINE_PROCESSED_COLOR);
                }
                else
                {
                    tvNextLine.Background = new SolidColorBrush(LINE_NOT_PROCESSED_COLOR);
                }
            }
            else
            {
                tvNextLine.Text = END_OF_FILE;
                tvNextLine.Background = new SolidColorBrush(FILE_EDGE_COLOR);
            }


            icTagOptionsBox.ItemsSource = TagLine.WordUnits;

            logger.LogMsg("Update the GUI for the Tag Line was successful.", EventLogger.EventType.Info);

        }

        public void NextLine(object sender, RoutedEventArgs e)
        {
            // Log the button press.
            EventLogger.instance().ButtonClickLog(sender);

            tvCurrentLine.Focus();
            try
            {
                ChildesFile.Next();
                UpdateTagLineView();
            }
            catch (IndexOutOfRangeException except)
            {
                ChildesFile.Previous();
            }

        }

        public void PreviousLine(object sender, RoutedEventArgs e)
        {
            // Log the button press.
            EventLogger.instance().ButtonClickLog(sender);

            tvCurrentLine.Focus();
            try
            {
                ChildesFile.Previous();
                UpdateTagLineView();
            }
            catch (IndexOutOfRangeException except)
            {
                ChildesFile.Next();
            }
        }


        void PreviousUnprocessed(object sender, RoutedEventArgs e)
        {
            // Log the button press.
            EventLogger.instance().ButtonClickLog(sender);

            tvCurrentLine.Focus();

            try
            {
                ChildesFile.PreviousUnprocessed();
                UpdateTagLineView();
            } catch (IndexOutOfRangeException exception)
            {
                ChildesFile.MoveToStart();
                UpdateTagLineView();
            }
            
        }

       

        /// <summary>
        /// The actual function to get the next unprocessed line,
        /// and then update the GUI for that tag line. 
        /// </summary>
        private void NextUnprocessedActual()
        {
            try
            {
                ChildesFile.NextUnprocessed();
                UpdateTagLineView();
            }
            catch (IndexOutOfRangeException exception)
            {
                ChildesFile.MoveToEnd();
                UpdateTagLineView();
            }
        }

        void NextUnprocessed(object sender, RoutedEventArgs e)
        {
            // Log the button press.
            EventLogger.instance().ButtonClickLog(sender);

            tvCurrentLine.Focus();
            NextUnprocessedActual();
        }

        private void SaveFileActual(String fileName)
        {
            try
            {

                ChildesFile.Save(fileName);
            }
            catch (Exception exception)
            {
                MessageBox.Show("An unexpected error occurred while saving the file. This most likely means the original file was not formatted correctly to begin with.");
            }

        }

        void SaveFile()
        {
            if (ChildesFile.FileOpened )
            {
                String fileName = ChildesFile.FileName;
                SaveFileActual(fileName);
            }

        }

        void SaveFile(object sender, RoutedEventArgs e)
        {

            EventLogger.instance().ButtonClickLog("File->Save");

            SaveFile();
        }
        void OpenFile(object sender, RoutedEventArgs e)
        {
            EventLogger logger = EventLogger.instance();

            logger.ButtonClickLog("File->Open");

            logger.LogMsg("Opening file dialog.", EventLogger.EventType.Info);


            OpenFileDialog dialog = new OpenFileDialog();

           

            dialog.ShowDialog(this);
            if (dialog.FileName != "") {
                try
                {
                    logger.LogMsg(String.Format("Attempting to open file {0}.", dialog.FileName), EventLogger.EventType.Event);
                    ChildesFile.Open(dialog.FileName);
                    OnFileOpened();

                } catch (Exception exception) {
                    logger.LogMsg(String.Format("Could not open file {0}.", dialog.FileName), EventLogger.EventType.Warning);
    
                    MessageBox.Show("An unexpected error occurred while opening the file. This most likely means the file was not formatted correctly.");
                    Reset();
                }

                // Write the native log into this application's log.
                logger.LogMsg("Dumping information from opening file.", EventLogger.EventType.Info);
                logger.ManualWrite(ChildesFile.DebugLog);
                ChildesFile.ClearNativeLog();

                if (ChildesFile.ReadingError)
                {

                   logger.LogMsg("An unexpected error occurred while opening the file. This most likely means the file was not formatted correctly. The file is still readable, but you should report this error.", EventLogger.EventType.Warning);
                    logger.LogMsg(String.Format("Errors in file {0}.", dialog.FileName), EventLogger.EventType.Warning);
                }

              
                
            }




        }

        /// <summary>
        /// Called when [ the line processed toggle button is clicked]. If the line was set to be processed, move to the next line.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        void OnLineProcessed(object sender, RoutedEventArgs e)
        {
            // Log the button press.
            EventLogger.instance().ButtonClickLog(sender);

            tvCurrentLine.Focus();
            ((ToggleButton)sender).GetBindingExpression(ToggleButton.IsCheckedProperty).UpdateSource();
            if ((bool)((ToggleButton)sender).IsChecked)
            {
                SaveFile();
                NextUnprocessedActual();
            }
        }

        /// <summary>
        /// Called when [ a file is opened successfully].
        /// </summary>
        void OnFileOpened()
        {
            EventLogger.instance().LogMsg(String.Format("File {0} was successfully opened.", ChildesFile.FileName), EventLogger.EventType.Event);

            tvTotalLine.Visibility = Visibility.Visible;
            tvLineNumberPrefix.Visibility = Visibility.Visible;
            tvTotalLinePrefix.Visibility = Visibility.Visible;
            tvTotalLine.Visibility = Visibility.Visible;
            teLineNumber.Visibility = Visibility.Visible;
            bNextLine.IsEnabled = true;
            bPreviousLine.IsEnabled = true;
            bPreviousUnprocessed.IsEnabled = true;
            bNextUnprocessed.IsEnabled = true;
            tbLineProcessed.IsEnabled = true;

            this.Title = System.IO.Path.GetFileName(ChildesFile.FileName);

            tvTotalLine.Text = ChildesFile.TagLineCount.ToString();
            UpdateTagLineView();

        }


        /// <summary>
        /// Displays the invalid line number error.
        /// </summary>
        private void DisplayInvalidLineNumberError()
        {
            EventLogger.instance().LogMsg("Invalid line number entered into " + teLineNumber.Name, EventLogger.EventType.Debug);

            MessageBox.Show("Invalid line number.");
            TagLineNumber = ChildesFile.Position + 1;
            this.teLineNumber.Text = TagLineNumber.ToString();


            EventLogger.instance().ValueLog("ChildesFile.Position", ChildesFile.Position.ToString());
        }

        /// <summary>
        /// Resets this instance, if a file is opened, call Close first.
        /// </summary>
        void Reset()
        {
            // Log that we are resetting the gui.
            EventLogger.instance().LogMsg("Resetting GUI", EventLogger.EventType.Event);

            this.Title = "Childes GUI";
            ChildesFile.Close(false);

            tvNextLine.Text = "";
            tvPreviousLine.Text = "";
            tvCurrentLine.Text = Childes_GUI_3.Properties.Resources.OpenFilePrompt;
            tvTotalLine.Visibility = Visibility.Hidden;
            tvLineNumberPrefix.Visibility = Visibility.Hidden;
            tvTotalLinePrefix.Visibility = Visibility.Hidden;
            tvTotalLine.Visibility = Visibility.Hidden;
            teLineNumber.Visibility = Visibility.Hidden;
        }


       

        /// <summary>
        /// Manually closes the file />.
        /// </summary>
        /// <returns>true if the file was closed, false if it was not.</returns>
        bool CloseFile()
        {

            EventLogger logger = EventLogger.instance();

            logger.LogMsg(String.Format("Closing file {0}.", ChildesFile.FileName), EventLogger.EventType.Event);
            if (ChildesFile.FileOpened)
            {
                MessageBoxResult result = MessageBox.Show(Childes_GUI_3.Properties.Resources.SaveBeforeCloseWarning, "", MessageBoxButton.YesNoCancel);
               switch (result)
                {
                    // Both "Yes" and "No" result in closing the file, 
                    // but "Yes" causes the file to get saved.
                    case MessageBoxResult.Yes:
                        SaveFile();
                        ChildesFile.Close(false);
                        return true;
                    case MessageBoxResult.No:
                        ChildesFile.Close(false);
                        return true;

                     // Everything else ought to do nothing.
                    default:
                        break;
                }
            }
            return false;

        }

        /// <summary>
        /// User interface accessible front for the close function
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        void CloseFile(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Handles the KeyDown event of the teLineNumber control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void teLineNumber_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Return || e.Key == Key.Enter)
            {

                // EventLogger messages.
                EventLogger logger = EventLogger.instance();
                logger.LogMsg("Enter pressed in teLineNumber.", EventLogger.EventType.Event);

                logger.ValueLog(teLineNumber.Name, teLineNumber.Text);



                int lineNumber;
                if (int.TryParse(teLineNumber.Text, out lineNumber))
                {
                    try
                    {
                        ChildesFile.Position = lineNumber - 1;
                        UpdateTagLineView();
                    }
                    catch (IndexOutOfRangeException exception)
                    {
                        DisplayInvalidLineNumberError();
                    }
                }
                else
                {
                    DisplayInvalidLineNumberError();
                }
            }
        }
        

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="b"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool b)
        {
            ChildesFile.Dispose();
            TagLine.Dispose();
        }


        /// <summary>
        /// Exits the window.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        void Exit(object sender, RoutedEventArgs e)
        {
            // Log the button press.
            EventLogger.instance().ButtonClickLog("File->Exit");
            Close();

        }

        
       


        void WindowClosing(object sender, CancelEventArgs e)
        {


            Properties.Settings.Default.Save();


            SaveFile();

            EventLogger.instance().finishLogging();

        }


    }


   


}
