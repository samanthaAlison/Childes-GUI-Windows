// EntryPoint.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


#include <ChildesTagLineBrowser.h>
#include "../ChildesFileReader/ChildesTagLineBrowser.h"
#include <fstream>
#include <iostream>

const std::wstring FOLDER(L"C:/Users/atFS/Documents/data/");

int main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
	_CrtSetBreakAlloc(73);
	CChildesReaderContext * context = new CChildesReaderContext(L"C:\\Users\\atFS\\Source\\Repos\\childes-gui\\ChildesFileReader\\FileWordBlackList.txt");
	CChildesTagLineBrowser * file = new CChildesTagLineBrowser(context);


	file->OpenFile(FOLDER  +L"YBM-2008-07-30-MI-01.inprep");
	

	file->SaveFile((FOLDER + L"YBM-2008-07-30-MI-01.inprep").c_str());
	delete file;
	delete context;
	_CrtDumpMemoryLeaks();

    return 0;
}

