#pragma once

#include <string>

namespace ChildesClassLibrary
{

	class ChildesInterfaceUtils
	{
	public:
		static System::String ^ WstringToSystemString(const std::wstring & str)
		{

			const wchar_t * wcstr = str.c_str();
			int len = wcslen(wcstr);

			// Convert from wchar_t * to char *.
			// Right now, the wchar_t has every second byte be a 0x00, 
			// so we have to convert it so the other bytes aren't lost.
			char * cstr = new char[len];

			wcstombs(cstr, wcstr, len);



			// Now convert that to unsigned bytes for our encoder.
			unsigned char * buffer = new unsigned char[len];
			memcpy_s(buffer, len, cstr, len);


			// Delete char *
			delete cstr;

			// Finally do the encoding.
			System::Text::UTF8Encoding encoding;
			System::String ^ sysString = encoding.GetString(buffer, wcslen(wcstr));

			delete buffer;

			return sysString;
		}
	};
}