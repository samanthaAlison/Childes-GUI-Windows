#pragma once
#include <ChildesReaderContext.h>

namespace ChildesClassLibrary {

	ref class ChildesErrorLogInterface
	{
	public:
		ChildesErrorLogInterface(CChildesReaderContext *context);

	};
}

