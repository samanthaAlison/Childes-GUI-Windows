#include "stdafx.h"
#include "ChildesTagLineInterface.h"
#include <locale>
#include <codecvt>


/// <summary>
/// Initializes a new instance of the <see cref="ChildesTagLineInterface"/> class.
/// </summary>
/// <param name="tagLine">The tag line this is an interface to.</param>
ChildesClassLibrary::ChildesTagLineInterface::ChildesTagLineInterface(CChildesTagLine * tagLine)
{
	mTagLine = tagLine;



	mWordUnits = gcnew List<ChildesWordUnitInterface^>();

	int index = 0;

	for (CChildesTagLine::Iter iter = mTagLine->begin(); iter != mTagLine->end(); ++iter)
	{
		mWordUnits->Add(gcnew ChildesWordUnitInterface(*iter, index));
		index++;
	}


}

ChildesClassLibrary::ChildesTagLineInterface::~ChildesTagLineInterface()
{
}


	

ChildesClassLibrary::ChildesWordUnitInterface ^ ChildesClassLibrary::ChildesTagLineInterface::GetWordUnit()
{
	CChildesTagLine::Iter iter = CChildesTagLine::Iter(mTagLine, mPosition);

	return  gcnew ChildesClassLibrary::ChildesWordUnitInterface(*iter, mPosition);
	
}

ChildesClassLibrary::ChildesWordUnitInterface ^ ChildesClassLibrary::ChildesTagLineInterface::GetWordUnit(int index)
{
	CChildesTagLine::Iter iter = CChildesTagLine::Iter(mTagLine, index);
	return  gcnew ChildesClassLibrary::ChildesWordUnitInterface(*iter, index);

}
