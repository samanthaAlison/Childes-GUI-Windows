#pragma once
#include <ChildesWordUnit.h>
#include "ChildesTagOptionInterface.h"
#include "ChildesInterfaceUtils.hpp"
using namespace System;
using namespace System::Collections::Generic;
namespace ChildesClassLibrary
{

	public ref class ChildesWordUnitInterface
	{

	private:
		/// <summary>
		/// The word unit this object is an interface to. 
		/// </summary>
		CChildesWordUnit * mWordUnit;

		/// <summary>
		/// The position in the list of tag options we are at. 
		/// </summary>
		int mPosition = 0;
		
		List<ChildesTagOptionInterface^>^ mTagOptions;

		/// <summary>
		/// The index this has in the tag line. 
		/// </summary>
		int mIndex;

	public:
		ChildesWordUnitInterface(CChildesWordUnit * unit, int index);
		virtual ~ChildesWordUnitInterface();
		
		/// <summary>
		/// Gets the dialogue word for this word unit.
		/// </summary>
		/// <returns>The dialogue word as a System::String ^</returns>
		String ^ GetDialogueWord() { return gcnew String(mWordUnit->GetDialogueWord().c_str()); }



		
		public: property String^ DialogueWord
		{
			String^ get() { 
				std::wstring str = mWordUnit->GetDialogueWord();
				return ChildesInterfaceUtils::WstringToSystemString(str);
			}
		}

		public: property List<ChildesTagOptionInterface ^>^ TagOptions
		{
			List<ChildesTagOptionInterface^>^ get() { return mTagOptions; }
		}

		public: property int Index
		{
			int get() { return mIndex; }
		}

		public: property bool Flagged
		{

			bool get() { return mWordUnit->IsFlagged(); }
			void set(bool flagged) { mWordUnit->SetFlagged(flagged); }
		}
	};

}