#pragma once

using namespace System;
using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;
#include <ChildesTagLine.h>
#include "ChildesWordUnitInterface.h"
#include "ChildesInterfaceUtils.hpp"
#include <cstdlib>

namespace ChildesClassLibrary {
	public ref class ChildesTagLineInterface
	{
	private:
		
		


		/// <summary>
		/// The tag line this class is an inteface to.
		/// </summary>
		CChildesTagLine * mTagLine;

		/// <summary>
		/// The position in the list of word units.
		/// </summary>
		int mPosition = 0;
		List<ChildesWordUnitInterface^>^ mWordUnits;
	public:
		ChildesTagLineInterface(CChildesTagLine * tagLine);
		virtual ~ChildesTagLineInterface();


		int GetWordUnitCount() { return mTagLine->GetSize(); }

		ChildesWordUnitInterface ^ GetWordUnit();
		ChildesWordUnitInterface ^ GetWordUnit(int index);

		property String ^ DialogueLine {
			String ^ get() {
				std::wstring str = mTagLine->GetDialogueLine();
				return ChildesInterfaceUtils::WstringToSystemString(str);
			}
		}

		property bool Processed 
		{
			bool get() { return mTagLine->IsProcessed(); }
			void set(bool processed) { mTagLine->SetProcessed(processed); }
		}

		public: property List<ChildesWordUnitInterface^>^ WordUnits {
			List<ChildesWordUnitInterface^>^ get() { return mWordUnits; }
		}

		

	};
}
