// ChildesClassLibrary.h

#pragma once

using namespace System;

#include <ChildesReaderContext.h>
#include <ChildesTagLineBrowser.h>
#include "ChildesTagLineInterface.h"
namespace ChildesClassLibrary {

	public ref class ChildesFileInterface
	{
	private:
		CChildesReaderContext *mContext;
		CChildesTagLineBrowser * mTagLineBrowser;
		void Open(const wchar_t * fileName);
		void Save(const wchar_t * fileName);

				
		/// <summary>
		/// True if this has opened a file.
		/// </summary>
		bool mFileOpened = false;

		/// <summary>
		/// The position in the tag line browser we are at.
		/// </summary>
		int mPosition = 0;

		ChildesClassLibrary::ChildesTagLineInterface ^ GetTagLine();
	public:
		ChildesFileInterface();
		~ChildesFileInterface();

		void Open(System::String ^fileName);
		void Save(System::String ^fileName);
		void Close(bool save);
	
		ChildesClassLibrary::ChildesTagLineInterface ^ GetTagLine(int index);

		ChildesClassLibrary::ChildesTagLineInterface ^ Next();
		ChildesClassLibrary::ChildesTagLineInterface ^ Previous();
		ChildesClassLibrary::ChildesTagLineInterface ^ NextUnprocessed();
		ChildesClassLibrary::ChildesTagLineInterface ^ PreviousUnprocessed();


		void MoveToStart() { mPosition = 0; }
		void MoveToEnd() { mPosition = TagLineCount - 1; }

		property int TagLineCount
		{
			int get() { return mTagLineBrowser->GetTagLineCount(); }
		}
		property int Position 
		{
			int get() { return mPosition; }
			void set(int position);
		}
		
		property ChildesTagLineInterface ^ TagLine
		{
			ChildesTagLineInterface^ get() { return GetTagLine(); }
		}

		property String ^ FileName
		{
			String^ get() { return gcnew String(mTagLineBrowser->GetFileName().c_str()); }
		}

		property bool NewFile
		{
			bool get() { return mTagLineBrowser->IsNewFile(); }
		}

		property bool FileOpened
		{
			bool get() { return mFileOpened; }
		}

		property String ^ DebugLog
		{
			String^ get() { std::wstring log = mContext->GetDebugLog(); return gcnew String(ChildesInterfaceUtils::WstringToSystemString(log)); }
		}

		property bool ReadingError
		{
			bool get() { return mTagLineBrowser->ReadingError(); }

		}

		void ClearNativeLog() { mContext->ClearDebugLog(); }

	};
}
