// This is the main DLL file.

#include "stdafx.h"

#include "ChildesClassLibrary.h"
#include <string>
#include < stdio.h >  
#include < stdlib.h >  
#include < vcclr.h >  
#include <exception>


/// <summary>
/// Finalizes an instance of the <see cref="ChildesFileInterface"/> class.
/// </summary>
ChildesClassLibrary::ChildesFileInterface::~ChildesFileInterface()
{
	delete mTagLineBrowser;
	delete mContext;
	
}

/// <summary>
/// Opens the specified file.
/// </summary>
/// <param name="fileName">Name of the file.</param>
void ChildesClassLibrary::ChildesFileInterface::Open(System::String ^fileName)
{
	
	pin_ptr<const wchar_t> wch = PtrToStringChars(fileName);
	try {
		Open(wch);
	}
	catch (std::exception &e) {
		throw gcnew System::Exception(gcnew String(e.what()));
	}



	mPosition = 0;
}

void ChildesClassLibrary::ChildesFileInterface::Save(System::String ^ fileName)
{
	if (fileName->Substring(fileName->Length - 4) != ".out")
		fileName + ".out";

	pin_ptr<const wchar_t> wch = PtrToStringChars(fileName);
	Save(wch);
}

/// <summary>
/// Closes the file this is an interface to.
/// </summary>
/// <param name="save">if set to <c>true</c> [save the file first].</param>
void ChildesClassLibrary::ChildesFileInterface::Close(bool save)
{
	if (mFileOpened && save)
	{
		this->Save(FileName);
	}
	
	delete mTagLineBrowser;
	delete mContext;
	mContext = new CChildesReaderContext();
	mTagLineBrowser = new CChildesTagLineBrowser(mContext);
	mFileOpened = false;
}



ChildesClassLibrary::ChildesTagLineInterface ^ ChildesClassLibrary::ChildesFileInterface::GetTagLine()
{
	CChildesTagLineBrowser::Iter iter = CChildesTagLineBrowser::Iter(mTagLineBrowser, mPosition);
	return gcnew ChildesTagLineInterface(*iter);

}

ChildesClassLibrary::ChildesTagLineInterface ^ ChildesClassLibrary::ChildesFileInterface::GetTagLine(int index)
{
	CChildesTagLineBrowser::Iter iter = CChildesTagLineBrowser::Iter(mTagLineBrowser, index);

	return gcnew ChildesTagLineInterface(*iter);
	// TODO: insert return statement here
}

/// <summary>
/// Moves to the next tagline.
/// </summary>
/// <returns>The next tag line</returns>
ChildesClassLibrary::ChildesTagLineInterface ^ ChildesClassLibrary::ChildesFileInterface::Next()
{

	mPosition++; 
	if (mPosition >= TagLineCount)
		throw gcnew System::IndexOutOfRangeException();
	return GetTagLine();
}

/// <summary>
/// Moves to the previous tagline.
/// </summary>
/// <returns>The previous tagline</returns>
ChildesClassLibrary::ChildesTagLineInterface ^ ChildesClassLibrary::ChildesFileInterface::Previous()
{
	mPosition--;
	if (mPosition < 0)
		throw gcnew System::IndexOutOfRangeException();
	return GetTagLine();
}

/// <summary>
/// Gets the next tag line that is unprocessed.
/// </summary>
/// <returns>A ChildesTagLineInterface to the next line that is unprocessed.</returns>
ChildesClassLibrary::ChildesTagLineInterface ^ ChildesClassLibrary::ChildesFileInterface::NextUnprocessed()
{
	CChildesTagLineBrowser::Iter iter = CChildesTagLineBrowser::Iter(mTagLineBrowser, mPosition);
	++iter;
	int position = mPosition + 1;

	while (iter != mTagLineBrowser->end() && (*iter)->IsProcessed())
	{
		++iter;
		position++;
	}

	if (!(iter != mTagLineBrowser->end()))
	{
		throw gcnew System::IndexOutOfRangeException(gcnew String("Reached the end of the file without finding an unprocessed tag line"));
	}
	else 
	{
		mPosition = position;
		return gcnew ChildesTagLineInterface(*iter);
	}
}

/// <summary>
/// Gets the previous tag line that is unprocessed.
/// </summary>
/// <returns>A ChildesTagLineInterface to that closest line to the current line and is unprocessed</returns>
ChildesClassLibrary::ChildesTagLineInterface ^ ChildesClassLibrary::ChildesFileInterface::PreviousUnprocessed()
{
	CChildesTagLineBrowser::Iter iter = CChildesTagLineBrowser::Iter(mTagLineBrowser, mPosition);
	--iter;
	int position = mPosition - 1;

	while (iter != mTagLineBrowser->rend() && (*iter)->IsProcessed())
	{
		--iter;
		position--;
	}

	if (!(iter != mTagLineBrowser->rend()))
	{
		throw gcnew System::IndexOutOfRangeException(gcnew String("Reached the end of the file without finding an unprocessed tag line"));
	}
	else
	{
		mPosition = position;
		return gcnew ChildesTagLineInterface(*iter);
	}	
}

/// <summary>
/// Opens the specified file.
/// </summary>
/// <param name="fileName">Name of the file.</param>
void ChildesClassLibrary::ChildesFileInterface::Open(const wchar_t * fileName)
{
	Close(false);
	std::wstring fileNameString(fileName);
	try {
		mTagLineBrowser->OpenFile(fileNameString);
		mFileOpened = true;
	} catch (std::exception e) {
		throw gcnew System::Exception(gcnew String(e.what()));
	}


}

void ChildesClassLibrary::ChildesFileInterface::Save(const wchar_t * fileName)
{
	mTagLineBrowser->SaveFile(fileName);
}

/// <summary>
/// Initializes a new instance of the <see cref="ChildesFileInterface"/> class.
/// </summary>
ChildesClassLibrary::ChildesFileInterface::ChildesFileInterface()
{
	mContext = new CChildesReaderContext();
	mTagLineBrowser = new CChildesTagLineBrowser(mContext);


}

/// <summary>
/// Sets the specified position of the tag line browser.
/// </summary>
/// <param name="position">The new position.</param>
void ChildesClassLibrary::ChildesFileInterface::Position::set(int position)
{
	if (position < 0 || position >= TagLineCount)
	{
		throw gcnew System::IndexOutOfRangeException("There is no tag line at that index.");
	}
	else
	{
		mPosition = position; 
	}
}



