#include "stdafx.h"
#include "ChildesWordUnitInterface.h"

using namespace ChildesClassLibrary;

/// <summary>
/// Initializes a new instance of the <see cref="ChildesWordUnitInterface"/> class.
/// </summary>
/// <param name="unit">The word unit this will be an interface to.</param>

ChildesClassLibrary::ChildesWordUnitInterface::ChildesWordUnitInterface(CChildesWordUnit * unit, int index)
{
	mWordUnit = unit;

	mIndex = index;
	mTagOptions = gcnew List<ChildesTagOptionInterface^>();

	for (CChildesWordUnit::Iter iter = mWordUnit->begin(); iter != mWordUnit->end(); ++iter)
	{
		mTagOptions->Add(gcnew ChildesTagOptionInterface(*iter, this));
	}

}

/// <summary>
/// Finalizes an instance of the <see cref="ChildesWordUnitInterface"/> class.
/// </summary>
ChildesClassLibrary::ChildesWordUnitInterface::~ChildesWordUnitInterface()
{
}

