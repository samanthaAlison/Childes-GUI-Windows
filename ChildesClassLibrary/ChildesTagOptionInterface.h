#pragma once
#include "ChildesInterfaceUtils.hpp"
#include <TagOption.h>
using namespace System;

namespace ChildesClassLibrary
{
	ref class ChildesWordUnitInterface;

	public ref class ChildesTagOptionInterface
	{
		/// <summary>
		/// The tag option this is an interface to.
		/// </summary>
		CTagOptionSimple * mTagOption;
				
		/// <summary>
		/// The word unit this is a part of.
		/// </summary>
		ChildesWordUnitInterface ^ mWordUnit;
		


	public:
		ChildesTagOptionInterface(CTagOptionSimple * tagOption, ChildesWordUnitInterface ^ wordUnit);
		~ChildesTagOptionInterface();

		public: property String^ TagName
		{
			String^ get() { 
				std::wstring tag = mTagOption->GetFullTag(true);
				tag = tag.substr(0, tag.find(L"="));
				return ChildesInterfaceUtils::WstringToSystemString(tag); 
			}

		}

		public: property bool InfChoice {
			bool get() { return  mTagOption->IsInfChoice(); }
			void set(bool choice) {
				mTagOption->SetInfChoice(choice);
			}
		}

		public: property int WordUnitIndex 
		{
			int get();
		}
	
	};

}
