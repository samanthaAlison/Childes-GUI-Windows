#include "stdafx.h"
#include "ChildesTagOptionInterface.h"
#include "ChildesWordUnitInterface.h"
using namespace ChildesClassLibrary;

ChildesTagOptionInterface::ChildesTagOptionInterface(CTagOptionSimple * tagOption, ChildesClassLibrary::ChildesWordUnitInterface ^ wordUnit)
{
	mTagOption = tagOption;
	mWordUnit = wordUnit;

}

ChildesTagOptionInterface::~ChildesTagOptionInterface()
{

}

int ChildesTagOptionInterface::WordUnitIndex::get()
{
	return mWordUnit->Index;
}