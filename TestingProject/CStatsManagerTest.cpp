#include "stdafx.h"
#include "CppUnitTest.h"
#include "ChildesReaderContext.h"
#include "StatsItem.h"
#include "StatsItemManager.h"
#include <string>
#include <vector>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
const std::wstring FIELDS[] = { L"name", L"id", L"phone number" };

typedef CStatsItem StatsItem;
typedef CStatsItemManager Manager;
namespace TestingProject
{
	TEST_CLASS(CStatsItemManagerTest)
	{
	public:



		CStatsItemManager * GetManager()
		{
			const std::vector<std::wstring> fields = std::vector<std::wstring>(std::begin(FIELDS), std::end(FIELDS));

			
			CStatsItemManager * manager = new CStatsItemManager(fields);
			return manager;
		}

		TEST_METHOD(TestCStatsItemManagerConstructor)
		{
			const std::vector<std::wstring> fields = std::vector<std::wstring>(std::begin(FIELDS), std::end(FIELDS));


			CStatsItemManager manager = CStatsItemManager(fields);


		}

		TEST_METHOD(TestCSIManagerAddItems)
		{
			Manager * manager = GetManager();

			Assert::IsTrue(manager->GetItemCount() == 0);

			CStatsItem * item = manager->ItemAt(L"First");

			Assert::IsTrue(manager->GetItemCount() == 1);

			CStatsItem * item2 = manager->ItemAt(L"Second");

			Assert::IsTrue(manager->GetItemCount() == 2);

			CStatsItem * item3 = manager->ItemAt(L"First");

			Assert::IsTrue(manager->GetItemCount() == 2);


		}


		TEST_METHOD(TestCIManagerEditFields)
		{
			Manager * manager = GetManager();

			CStatsItem * item = manager->ItemAt(L"First");

			Assert::IsTrue((*item)[L"name"].GetValue() == 0);

			CStatsItem * item3 = manager->ItemAt(L"First");

			((*item3)[L"name"])++;

			Assert::IsTrue((*item)[L"name"].GetValue() == 1);

		}

		

	};
}