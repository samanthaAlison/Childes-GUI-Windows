#include "stdafx.h"
#include "CppUnitTest.h"
#include "ChildesReaderContext.h"
#include "StatsItem.h"
#include "StatsItemManager.h"
#include "StatsItemInstance.h"
#include "MultipleStatsItemInstance.h"
#include <string>
#include <vector>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
const std::wstring FIELDS[] = { L"name", L"id", L"phone number" };

typedef CStatsItem StatsItem;
typedef CStatsItemManager Manager;
typedef CStatsItemInstance Instance;
typedef CMultipleStatsItemInstance MInstance;

namespace TestingProject
{
	TEST_CLASS(CStatsItemInstanceTest)
	{
	public:




		CStatsItemManager * GetManager()
		{
			const std::vector<std::wstring> fields = std::vector<std::wstring>(std::begin(FIELDS), std::end(FIELDS));


			CStatsItemManager * manager = new CStatsItemManager(fields);

			CStatsItem * item = manager->ItemAt(L"First");



			CStatsItem * item2 = manager->ItemAt(L"Second");


			CStatsItem * item3 = manager->ItemAt(L"Third");

			return manager;
		}

		CStatsItemInstance * GetInstance(std::wstring id, Manager * manager)
		{
			CStatsItemInstance * instance = new CStatsItemInstance(id, manager);
			return instance;
		}

		MInstance * GetMInstance(MInstance * instance)
		{
			instance->IncrementUsageCount();
			return instance;
		}

		TEST_METHOD(TestCMultipleSIIConstructor)
		{


			CStatsItemManager * manager = GetManager();

			MInstance * instance = new MInstance(L"First", manager);
			
			delete manager;
		}

		TEST_METHOD(TestCMultipleSIIDestructor)
		{
			CStatsItemManager * manager = GetManager();

			MInstance * instance = new MInstance(L"First", manager);

			delete instance;
			delete manager;
		}

		TEST_METHOD(TestCMultipleSIIUsageCount)
		{
			CStatsItemManager * manager = GetManager();

			MInstance * instance = new MInstance(L"First", manager);

			Assert::IsTrue(instance->GetUsageCount() == 0);
			Assert::IsTrue(instance->GetCorrectChoiceCount() == 0);

			MInstance * instance2 = GetMInstance(instance);

			MInstance * instance3 = GetMInstance(instance);

			Assert::IsTrue(instance->GetUsageCount() == 2);
			Assert::IsTrue(instance->GetCorrectChoiceCount() == -2);
			Assert::IsFalse(instance->IsCorrectChoice());


			instance3->IncrementCorrectChoiceCount();

			Assert::IsTrue(instance->IsCorrectChoice());

			delete instance;

		}
	};
}