#include "stdafx.h"
#include "CppUnitTest.h"
#include "ChildesReaderContext.h"
#include "StatsItem.h"
#include "StatsItemManager.h"
#include "StatsItemInstance.h"
#include <string>
#include <vector>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
const std::wstring FIELDS[] = { L"name", L"id", L"phone number" };

typedef CStatsItem StatsItem;
typedef CStatsItemManager Manager;
typedef CStatsItemInstance Instance;
namespace TestingProject
{
	TEST_CLASS(CStatsItemInstanceTest)
	{
	public:



		CStatsItemManager * GetManager()
		{
			const std::vector<std::wstring> fields = std::vector<std::wstring>(std::begin(FIELDS), std::end(FIELDS));


			CStatsItemManager * manager = new CStatsItemManager(fields);

			CStatsItem * item = manager->ItemAt(L"First");



			CStatsItem * item2 = manager->ItemAt(L"Second");


			CStatsItem * item3 = manager->ItemAt(L"Third");

			return manager;
		}

		CStatsItemInstance * GetInstance(std::wstring id, Manager * manager)
		{
			CStatsItemInstance * instance = new CStatsItemInstance(id, manager);
			return instance;
		}

		TEST_METHOD(TestCStatsItemInstanceConstructor)
		{
			

			CStatsItemManager * manager = GetManager();

			Instance  * instance = new CStatsItemInstance(L"First", manager);
		}

		TEST_METHOD(TestCSIIAdjustValues)
		{
			CStatsItemManager * manager = GetManager();

			Instance * instance = GetInstance(L"First", manager);

			Instance * instance2 = GetInstance(L"First", manager);

			StatsItem * item1 = instance->GetStatsItem();

			StatsItem * item2 = instance2->GetStatsItem();

			Assert::IsTrue((*item1)[L"name"].GetValue() == 0);

			Assert::IsTrue((*item1)[L"name"].GetValue() == (*item2)[L"name"].GetValue());

			(*item1)[L"name"]++;

			Assert::IsTrue((*item1)[L"name"].GetValue() == 1);

			Assert::IsTrue((*item1)[L"name"].GetValue() == (*item2)[L"name"].GetValue());

		}
	};
}