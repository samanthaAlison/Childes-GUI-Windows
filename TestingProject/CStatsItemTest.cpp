#include "stdafx.h"
#include "CppUnitTest.h"
#include "ChildesReaderContext.h"
#include "StatsItem.h"
#include <string>
#include <vector>
#include <iterator>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
const std::wstring FIELDS[] = { L"name", L"id", L"phone number" };

typedef CStatsItem StatsItem;
namespace TestingProject
{		
	TEST_CLASS(CStatsItemTest)
	{
	public:
		


		StatsItem * GetItem()
		{
			const std::vector<std::wstring> fields = std::vector<std::wstring>(std::begin(FIELDS), std::end(FIELDS));

			StatsItem * item = new StatsItem(fields);
			return item;
		}

		TEST_METHOD(TestCStatsItemConstructor)
		{
			std::vector<std::wstring> fields = std::vector<std::wstring>(std::begin(FIELDS), std::end(FIELDS));
			
			CStatsItem item = StatsItem(fields);


		}

		TEST_METHOD(CStatsItemCopyConstructor)
		{
			std::vector<std::wstring> fields = std::vector<std::wstring>(std::begin(FIELDS), std::end(FIELDS));

			StatsItem item = StatsItem(fields);

			StatsItem item2 = item;

			int i = item[L"name"].GetValue();
			int j = item2[L"name"].GetValue();

			Assert::IsTrue(i == j);
			

		}

		TEST_METHOD(CStatsItemNewConstructor)
		{
			StatsItem * item = GetItem();

			StatsItem item2 = (*item);
			StatsItem * item3 = new StatsItem(*item);

		}


		TEST_METHOD(CStatsItemFieldAccess)
		{
			StatsItem * item = GetItem();
			
			

			for (std::wstring str : FIELDS)
			{
				Assert::IsTrue(0 == (*item)[str].GetValue());
			}

		}

	};
}