#include "TagOption.h"
#include "TagInstance.h"
#include "MorphInstanceFactory.h"
#include "MorphemeInstance.h"
#include <sstream>


/// <summary>
/// Initializes a new instance of the <see cref="CTagOption"/> class.
/// </summary>
/// <param name="morphemeText">The morpheme text.</param>
/// <param name="instance">The tagInstance this option represents.</param>
/// <param name="factory">The morpheme factory.</param>
CTagOption::CTagOption(std::wstring morphemeText, CTagInstance * instance, CMorphInstanceFactory * factory) : CTagOptionSimple(instance->GetId(), morphemeText)
{

	mWord = factory->CreateWord(morphemeText);
	mTagInstance = instance;

	mTagInstance->IncrementUsageCount();
}


CTagOption::~CTagOption()
{
}

/// <summary>
/// Sets this option as the option chosen by POST, and adjusts the statistics accordingly.
/// </summary>
/// <param name="post">if set to <c>true</c> [post].</param>
void CTagOption::SetPostChoice(bool post)
{
	// Only do something if we are actuall changing the value.
	if (IsPostChoice() != post) {
		CTagOptionSimple::SetPostChoice(post);
	}
}




