#pragma once
#include "MultipleStatsItemInstance.h"
class CMorphInstanceFactory;
/// <summary>
/// A morpheme in the MOR tags. Since a morpheme can be used in multiple tags
/// for the same WordUnit, there is only one copy of each morpheme for the whole
/// WordUnit, and ways to make sure that it does not skew the statistics when there
/// are multiple instances of the same morpheme.
/// </summary>
/// <seealso cref="CMultipleStatsItemInstance" />
/// <seealso cref="CStatsItemInstance" />
class CMorphemeInstance :
	public CMultipleStatsItemInstance
{
private:	
	
	/// <summary>
	/// True if this morpheme is a free morpheme.
	/// </summary>
	bool mFree;
	
	/// <summary>
	/// The factory we will get morpheme objects from.
	/// </summary>
	CMorphInstanceFactory * mFactory;
	
	/// <summary>
	/// The children morphemes that make up this morpheme.
	/// </summary>
	std::vector<CMorphemeInstance *> mChildren;
public:
	CMorphemeInstance(std::wstring id, CMorphInstanceFactory * factory, bool free = false);
	virtual ~CMorphemeInstance();
	
	
};

