<?xml version="1.0"?><doc>
<members>
<member name="T:CStatsItem" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="19">
<summary>
A class with integer stats we can keep track of, using text identifiers for each
individual field.
</summary>
<remarks>
Note that, although you can access the fields of this object like 
a map, IT IS NOT A MAP.Think of this more as an entry of a SQL
table; you must define your fields beforehand, and not add new
ones dynamically.This is why the only constructors available
to this class are protected and require field names.
</remarks>
</member>
<member name="F:CStatsItem.mManager" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="35">
<summary>
The stats manager that owns this.
</summary>
</member>
<member name="T:CStatsItem.StatsField" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="41">
<summary>
 A single field with a single numeric value.

We can read the value, increment it, and
decrement it
</summary>
</member>
<member name="F:CStatsItem.StatsField.mValue" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="50">
The value we are keeping track of.
</member>
<member name="M:CStatsItem.StatsField.op_Increment" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="61">
<summary>
Increments the StatsField's value.
</summary>
<returns>The StatsField after incrementing the value.</returns>
</member>
<member name="M:CStatsItem.StatsField.op_Increment(System.Int32)" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="67">
<summary>
Increments the StatsField's value.
</summary>
<returns>The StatsField before incrementing the value.</returns>
</member>
<member name="M:CStatsItem.StatsField.op_Decrement" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="73">
<summary>
Decrements the StatsField's value.
</summary>
<returns>The StatsField after decrementing the value.</returns>
</member>
<member name="M:CStatsItem.StatsField.op_Decrement(System.Int32)" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="79">
<summary>
Decrements the StatsField's value.
</summary>
<returns>The StatsField before decrementing the value.</returns>
</member>
<member name="M:CStatsItem.StatsField.GetValue" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="85">
<summary>
Gets the value of the StatsField.
</summary>
<returns>The value of the StatsField.</returns>
</member>
<member name="F:CStatsItem.mStatsFields" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="96">
<sumary>
The fields this item will be keeping track of.
</sumary>
</member>
<member name="M:CStatsItem.#ctor(CStatsItem!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="105">
<summary>
Initializes a new instance of the <see cref="T:CStatsItem"/> class via 
the copy constructor.
</summary>
<param name="item">The item we are copying.</param>
</member>
<member name="M:CStatsItem.SetManager(CStatsItemManager*)" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="117">
<summary>
Sets the manager of this class.
</summary>
<param name="manager">The new manager.</param>
</member>
<member name="M:CStatsItem.op_Subscript(std.basic_string&lt;System.Char,std.char_traits{System.Char},std.allocator&lt;System.Char&gt;&gt;)" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="124">
<summary>
Get a StatsField object from this item using 
map-like syntax. Fails an assertion if
you try to use it for a field that doesn't exist
so be careful.
</summary>
<param name="fieldName">Name of the field we are looking for.</param>
<returns>The reference to the StatsField object, if it passes the assertion.</returns>
</member>
<member name="T:__type_info_node" decl="true" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitemmanager.h" line="1">
 \file StatsItemManager.h

 \author Samantha Oldenburg

 Describes a class that keeps track of objects of a single 
 type derived from the StatsItem class.


</member>
<member name="T:CStatsItemManager" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitemmanager.h" line="22">
Class that keeps track of StatsItem objects.

</member>
<member name="F:CStatsItemManager.mPrototype" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitemmanager.h" line="29">
This item is the item that all items in this
manager will be based off of. That means that
every new item this manager creates will have
the same fields as this item does. 
</member>
<member name="F:CStatsItemManager.mStatsItems" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitemmanager.h" line="35">
<summary>
The stats items.
</summary>
</member>
<member name="M:CStatsItemManager.GetItemCount" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitemmanager.h" line="48">
<summary>
Gets the StatsItem count.
</summary>
<returns>The amount of items this manager has.</returns>
</member>
<member name="T:CStatsItemInstance" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsiteminstance.h" line="4">
<summary>
An instance of the CStatsItem classed. Used as an intermediate class
in a flyweight-like way.
</summary>
</member>
<member name="F:CStatsItemInstance.mStatsItem" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsiteminstance.h" line="11">
<summary>
The pointer to the StatsItem object this object is an instance of.
</summary>
</member>
<member name="F:CStatsItemInstance.mId" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsiteminstance.h" line="16">
<summary>
The  identifier of the CStatsItem this is an instance of.
</summary>
</member>
<member name="M:CStatsItemInstance.GetId" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsiteminstance.h" line="26">
<summary>
Gets the identifier.
</summary>
<returns>The identifier string</returns>
</member>
<member name="M:CStatsItemInstance.GetStatsItem" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsiteminstance.h" line="34">
<summary>
Gets the stats item.
</summary>
<returns>The stats item.</returns>
</member>
<member name="T:CMultipleStatsItemInstance" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\multiplestatsiteminstance.h" line="3">
<summary>
A stats item usage, but used in a case where a single
stats item could appear multiple times in the same context,
and only needs to be selected in one of those instances to be
selected overall.
</summary>
<seealso cref="T:CStatsItemInstance"/>
</member>
<member name="F:CMultipleStatsItemInstance.mUsageCount" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\multiplestatsiteminstance.h" line="14">
<summary>
The amount of instances of this StatsItem being used.
</summary>
</member>
<member name="F:CMultipleStatsItemInstance.mCorrectChoiceCount" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\multiplestatsiteminstance.h" line="19">
<summary>
A number that decreases when a StatsItem was not selected but increases when 
it is selected. Overall, <c>mUsageCount</c> + <c>mCorrectChoiceCount</c> is the 
amount of times the StatsItem was selected in this context.
</summary>
</member>
<member name="M:CMultipleStatsItemInstance.IncrementUsageCount" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\multiplestatsiteminstance.h" line="34">
<summary>
Increments the usage count. Called if a an uses
this instance's CStatsItem.
</summary>
</member>
<member name="M:CMultipleStatsItemInstance.DecrementCorrectChoiceCount" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\multiplestatsiteminstance.h" line="46">
<summary>
Decrements the correct choice count.
</summary>
</member>
<member name="M:CMultipleStatsItemInstance.IncrementCorrectChoiceCount" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\multiplestatsiteminstance.h" line="51">
<summary>
Increments the correct choice count.
</summary>
</member>
<member name="M:CMultipleStatsItemInstance.IsCorrectChoice" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\multiplestatsiteminstance.h" line="56">
<summary>
Determines whether [this instance's CStatsItem was selected].
</summary>
<returns>
  <c>true</c> if [it is correct choice]; otherwise, <c>false</c>.
</returns>
</member>
<member name="M:CMultipleStatsItemInstance.GetUsageCount" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\multiplestatsiteminstance.h" line="64">
<summary>
Gets the amount of times this instance's CStatsItem has been used in this context.
</summary>
<returns>The usage count</returns>
</member>
<member name="M:CMultipleStatsItemInstance.GetCorrectChoiceCount" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\multiplestatsiteminstance.h" line="70">
<summary>
Gets the amount of times this this StatsItem was selected as correct
</summary>
<returns></returns>
</member>
<member name="T:CMorphemeInstance" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\morphemeinstance.h" line="4">
<summary>
A morpheme in the MOR tags. Since a morpheme can be used in multiple tags
for the same WordUnit, there is only one copy of each morpheme for the whole
WordUnit, and ways to make sure that it does not skew the statistics when there
are multiple instances of the same morpheme.
</summary>
<seealso cref="T:CMultipleStatsItemInstance"/>
<seealso cref="T:CStatsItemInstance"/>
</member>
<member name="F:CMorphemeInstance.mFree" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\morphemeinstance.h" line="17">
<summary>
True if this morpheme is a free morpheme.
</summary>
</member>
<member name="F:CMorphemeInstance.mFactory" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\morphemeinstance.h" line="22">
<summary>
The factory we will get morpheme objects from.
</summary>
</member>
<member name="F:CMorphemeInstance.mChildren" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\morphemeinstance.h" line="27">
<summary>
The children morphemes that make up this morpheme.
</summary>
</member>
<member name="T:CMorphInstanceFactory" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\morphinstancefactory.h" line="6">
<summary>
A factory that produces morpheme instances.
It is vital to being able to maintain just one instance of 
a morpheme for an entire ChildesWordUnit. Just because a 
a word a certain morpheme was in wasn't selected, doesn't
mean that the morpheme was incorrected selected. 
</summary>
</member>
<member name="F:CMorphInstanceFactory.mMorphemeManager" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\morphinstancefactory.h" line="15">
<summary>
The morpheme manager we will be getting the Morpheme items from
They are CStatsItems
</summary>
</member>
<member name="F:CMorphInstanceFactory.mInstances" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\morphinstancefactory.h" line="20">
<summary>
The morpheme instances of this unit.
The key of the map is the text that makes up the morpheme. 
</summary>
</member>
<member name="M:CMorphInstanceFactory.GetManager" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\morphinstancefactory.h" line="38">
<summary>
Gets the manager.
</summary>
<returns>The morpheme manager</returns>
</member>
<member name="M:CMorphemeInstance.#ctor(std.basic_string&lt;System.Char,std.char_traits{System.Char},std.allocator&lt;System.Char&gt;&gt;,CMorphInstanceFactory*,System.Boolean)" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\morphemeinstance.cpp" line="5">
<summary>
Initializes a new instance of the <see cref="T:CMorphemeInstance"/> class.
</summary>
<param name="id">The identifier.</param>
<param name="factory">The factory that creates morphemes.</param>
<param name="free">if set to <c>true</c> [free].</param>
</member>
<member name="M:CMorphemeInstance.Dispose" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\morphemeinstance.cpp" line="17">
<summary>
Finalizes an instance of the <see cref="T:CMorphemeInstance"/> class.
</summary>
</member>
</members>
</doc>