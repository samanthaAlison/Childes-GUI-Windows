<?xml version="1.0"?><doc>
<members>
<member name="F:CTagOptionSimple.mInfChoice" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\tagoptionsimple.h" line="7">
<summary>
Is true when the informant chooses this tag.
</summary>
</member>
<member name="F:CTagOptionSimple.mPostChoice" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\tagoptionsimple.h" line="12">
<summary>
Is true if POST chose this tag.
</summary>
</member>
<member name="F:CTagOptionSimple.mTagId" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\tagoptionsimple.h" line="18">
<summary>
The tag identifier
</summary>
</member>
<member name="F:CTagOptionSimple.mWord" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\tagoptionsimple.h" line="23">
<summary>
The word this tag option defines.
</summary>
</member>
<member name="M:CTagOptionSimple.SetInfChoice(System.Boolean)" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\tagoptionsimple.h" line="33">
<summary>
Sets the inf choice.
</summary>
<param name="choice">if set to <c>true</c> [this is the inf choice].</param>
</member>
<member name="M:CTagOptionSimple.SetPostChoice(System.Boolean)" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\tagoptionsimple.h" line="39">
<summary>
Sets the POST choice.
</summary>
<param name="choice">if set to <c>true</c> [this is the POST choice].</param>
</member>
<member name="M:CTagOptionSimple.IsInfChoice" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\tagoptionsimple.h" line="45">
<summary>
Determines whether this option [is inf choice].
</summary>
<returns>
  <c>true</c> if [is inf choice]; otherwise, <c>false</c>.
</returns>
</member>
<member name="M:CTagOptionSimple.IsPostChoice" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\tagoptionsimple.h" line="53">
<summary>
Determines whether [this option is the POST choice].
</summary>
<returns>
  <c>true</c> if [is POST choice]; otherwise, <c>false</c>.
</returns>
</member>
<member name="M:CTagOptionSimple.GetTagId" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\tagoptionsimple.cpp" line="16">
<summary>
Gets the tag identifier of the tag this option represents an instance of.
</summary>
<returns>The identifying string of the tag. </returns>
</member>
<member name="M:CTagOptionSimple.GetFullTag" decl="false" source="c:\users\samantha\source\repos\childes-gui\taglib\tagoptionsimple.cpp" line="25">
<summary>
Gets the full tag option as it would appear in the MOR line.
</summary>
<returns></returns>
</member>
</members>
</doc>