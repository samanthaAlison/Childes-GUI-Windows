#include "TagInstance.h"


/// <summary>
/// Finalizes an instance of the <see cref="CTagInstance"/> class.
/// </summary>
/// <param name="">The identifier of this tag instance.</param>
/// <param name="tagManager">The tag manager we will use to get the tag itself.</param>
/// <param name="factory">The factory used to get the morphemes.</param>
CTagInstance::CTagInstance(std::wstring id, CStatsItemManager * tagManager, CMorphInstanceFactory * factory) : 
	CMultipleStatsItemInstance(id, tagManager)
{
	
}

/// <summary>
/// Destructor of the <see cref="CTagInstance"/> class.
/// </summary>
CTagInstance::~CTagInstance()
{
}

/// <summary>
/// Analyzes the usage of this instance and update the statistics of the item this is an instance of. 
/// </summary>
void CTagInstance::AnalyzeStats()
{
	
}
