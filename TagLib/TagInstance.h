#pragma once

#include "MultipleStatsItemInstance.h"
#include "MorphInstanceFactory.h"
#include <string>

class CTagInstance :
	public CMultipleStatsItemInstance
{
private:	
	
public:
	CTagInstance(std::wstring, CStatsItemManager * tagManager, CMorphInstanceFactory * factory);
	~CTagInstance();	
	CTagInstance() = delete;
	CTagInstance(const CTagInstance &) = delete;
	virtual void AnalyzeStats() override;
	
};

