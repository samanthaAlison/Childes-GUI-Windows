#pragma once
#include "StatsItemManager.h"
#include <map>
class CMorphemeWord;
class CMorphemeInstance;
/// <summary>
/// A factory that produces morpheme instances.
/// It is vital to being able to maintain just one instance of 
/// a morpheme for an entire ChildesWordUnit. Just because a 
/// a word a certain morpheme was in wasn't selected, doesn't
/// mean that the morpheme was incorrected selected. 
/// </summary>
class CMorphInstanceFactory
{		
	/// <summary>
	/// The morpheme manager we will be getting the Morpheme items from
	/// They are CStatsItems
	/// </summary>
	CStatsItemManager * mMorphemeManager;	
	/// <summary>
	/// The morpheme instances of this unit.
	/// The key of the map is the text that makes up the morpheme. 
	/// </summary>
	std::map<std::wstring, CMorphemeInstance* > mInstances;

	CMorphemeInstance * AddInstance(std::wstring id, bool word = false);
	//CMorphemeInstance * AddInstance(std::wstring id, CMorphemeInstance * instance);
public:
	CMorphInstanceFactory(CStatsItemManager * morphemeManager);
	CMorphInstanceFactory() = delete;
	CMorphInstanceFactory(const CMorphInstanceFactory & other);
	virtual ~CMorphInstanceFactory();

	CMorphemeInstance * GetMorpheme(std::wstring id);

	CMorphemeInstance * CreateWord(std::wstring id);
	
	/// <summary>
	/// Gets the manager.
	/// </summary>
	/// <returns>The morpheme manager</returns>
	CStatsItemManager * GetManager() { return mMorphemeManager; }
};

