#include "MorphInstanceFactory.h"
#include "MorphemeInstance.h"
#include "MorphemeWord.h"
/// <summary>
/// Adds a morpheme instance where one did not exist or return one if it already exists.
/// </summary>
/// <param name="id">The identifier of the new morpheme.</param>
/// <param name="word">True if the morpheme will be a free morpheme.</param>
/// <returns></returns>
CMorphemeInstance * CMorphInstanceFactory::AddInstance(std::wstring id, bool word)
{
	std::map<std::wstring, CMorphemeInstance *>::iterator iter = mInstances.find(id);
	
	// Replacing words we already have is a good way to cause memory leaks and stack corruption. 
	// Let's make sure NOT to do that. 
	if (iter == mInstances.end())
	{
		CMorphemeInstance * instance = new CMorphemeInstance(id, this, word);
		mInstances.insert(std::make_pair(id, instance));
		return instance;
	}
	else
	{
		return iter->second;
	}
}


/*/// <summary>
/// Adds a morpheme instance where one did not exist.
/// </summary>
/// <param name="id">The identifier of the new morpheme.</param>
/// <param name="instance">Instance we are adding.</param>
/// <returns></returns>
CMorphemeInstance * CMorphInstanceFactory::AddInstance(std::wstring id, CMorphemeInstance * instance)
{
	std::map<std::wstring, CMorphemeInstance *>::iterator iter = mInstances.find(id);

	// Replacing words we already have is a good way to cause memory leaks and stack corruption. 
	// Let's make sure NOT to do that.
	if (iter == mInstances.end())
	{
		mInstances.insert(std::make_pair(id, instance));
		return instance;
	}
	else
	{
		//delete instance;
		return iter->second;
	}
	
}
*/



/// <summary>
/// Finalizes an instance of the <see cref="CMorphInstanceFactory"/> class.
/// </summary>
/// <param name="morphemeManager">The CStatsItemManager that this will get the morphemes from.</param>
CMorphInstanceFactory::CMorphInstanceFactory(CStatsItemManager * morphemeManager)
{
	mMorphemeManager = morphemeManager;
}

CMorphInstanceFactory::CMorphInstanceFactory(const CMorphInstanceFactory & other)
{
	mMorphemeManager = other.mMorphemeManager;
	mInstances.insert(other.mInstances.begin(), other.mInstances.end());
}

/// <summary>
/// Destructor of the <see cref="CMorphInstanceFactory"/> class.
/// </summary>
CMorphInstanceFactory::~CMorphInstanceFactory()
{
	std::map<std::wstring, CMorphemeInstance *>::iterator iter = mInstances.begin();
	for (; iter != mInstances.end(); ++iter)
	{
		delete (iter->second);
	}
}

/// <summary>
/// Gets the morpheme with the specified identifier.
/// </summary>
/// <param name="id">The identifier.</param>
/// <returns></returns>
CMorphemeInstance * CMorphInstanceFactory::GetMorpheme(std::wstring id)
{
	std::map<std::wstring, CMorphemeInstance *>::iterator iter = mInstances.find(id);
	if (iter != mInstances.end())
	{
		return iter->second;
	}
	else
	{
		return AddInstance(id);
	}
}

CMorphemeInstance * CMorphInstanceFactory::CreateWord(std::wstring id)
{
	CMorphemeInstance * word = AddInstance(id);
	word->IncrementUsageCount();
	return word;
}
