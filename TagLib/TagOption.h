#pragma once
#include "TagOptionSimple.h"
#include <string>
class CTagInstance;
class CMorphemeInstance;
class CMorphInstanceFactory;
/// <summary>
/// An option that MOR Generated. Holds the tag instance
/// </summary>
class CTagOption: public CTagOptionSimple
{
private:


	/// <summary>
	/// The word this tag represents.
	/// </summary>
	CMorphemeInstance * mWord = nullptr;
	/// <summary>
	/// The TagInstance this option represents.
	/// </summary>
	CTagInstance * mTagInstance = nullptr;


public:
	CTagOption(std::wstring morphemeText, CTagInstance * instance, CMorphInstanceFactory * factory);
	virtual ~CTagOption();
	CTagOption() = delete;
	CTagOption(const CTagOption &) = delete;
	
	virtual void SetPostChoice(bool post) override;

};

