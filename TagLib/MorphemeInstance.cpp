#include "MorphemeInstance.h"
#include "MorphInstanceFactory.h"


/// <summary>
/// Initializes a new instance of the <see cref="CMorphemeInstance"/> class.
/// </summary>
/// <param name="id">The identifier.</param>
/// <param name="factory">The factory that creates morphemes.</param>
/// <param name="free">if set to <c>true</c> [free].</param>
CMorphemeInstance::CMorphemeInstance(std::wstring id, CMorphInstanceFactory * factory, bool free) : CMultipleStatsItemInstance(id, factory->GetManager())
{
	mFree = free;

}

/// <summary>
/// Finalizes an instance of the <see cref="CMorphemeInstance"/> class.
/// </summary>
CMorphemeInstance::~CMorphemeInstance()
{
}
