#pragma once
#include <string>
class CTagOptionSimple
{

private:
	/// <summary>
	/// Is true when the informant chooses this tag.
	/// </summary>
	bool mInfChoice = false;

	/// <summary>
	/// Is true if POST chose this tag.
	/// </summary>
	bool mPostChoice = false;

	
	/// <summary>
	/// The tag identifier
	/// </summary>
	std::wstring mTagId;
	
	/// <summary>
	/// The word this tag option defines.
	/// </summary>
	std::wstring mWord;

public:
	CTagOptionSimple(std::wstring tag, std::wstring word);
	~CTagOptionSimple();


	/// <summary>
	/// Sets the inf choice.
	/// </summary>
	/// <param name="choice">if set to <c>true</c> [this is the inf choice].</param>
	void SetInfChoice(bool choice) { mInfChoice = choice; }

	/// <summary>
	/// Sets the POST choice.
	/// </summary>
	/// <param name="choice">if set to <c>true</c> [this is the POST choice].</param>
	virtual void SetPostChoice(bool choice) { mPostChoice = choice; }

	/// <summary>
	/// Determines whether this option [is inf choice].
	/// </summary>
	/// <returns>
	///   <c>true</c> if [is inf choice]; otherwise, <c>false</c>.
	/// </returns>
	bool IsInfChoice() { return mInfChoice; }

	/// <summary>
	/// Determines whether [this option is the POST choice].
	/// </summary>
	/// <returns>
	///   <c>true</c> if [is POST choice]; otherwise, <c>false</c>.
	/// </returns>
	bool IsPostChoice() { return mPostChoice; }

	std::wstring GetTagId();

	std::wstring GetFullTag(bool nice = false);


};

