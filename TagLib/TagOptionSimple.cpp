#include "TagOptionSimple.h"
#include <sstream>

/// <summary>
/// Initializes a new instance of the <see cref="CTagOptionSimple"/> class.
/// </summary>
/// <param name="tag">The tag.</param>
/// <param name="word">The word.</param>
CTagOptionSimple::CTagOptionSimple(std::wstring tag, std::wstring word)
{
	mTagId = tag;
	mWord = word;
}


/// <summary>
/// Finalizes an instance of the <see cref="CTagOptionSimple"/> class.
/// </summary>
CTagOptionSimple::~CTagOptionSimple()
{
}


/// <summary>
/// Gets the tag identifier of the tag this option represents an instance of.
/// </summary>
/// <returns>The identifying string of the tag. </returns>
std::wstring CTagOptionSimple::GetTagId()
{
	return mTagId;
}

/// <summary>
/// Gets the full tag option as it would appear in the MOR line.
/// </summary>
/// <param name="nice">If set to true, space out the tag to make it look nice.</param>
/// <returns></returns>
std::wstring CTagOptionSimple::GetFullTag(bool nice)
{
	std::wstring separtor;
	std::wstring word;
	
	
	if (nice)
	{
		// Give the tag nice spacing. 
		separtor = L" | ";
		std::wostringstream oss = std::wostringstream(L"");
		for (wchar_t wc : mWord)
		{
			
			if (wc == L'|' || wc == L'&' || wc == L'~' || wc == L'-')
			{
				oss << L' ' << wc << L' ';
			}
			else
			{
				oss << wc;
			}
		}
		word = oss.str();
	}
	else
	{
		word = mWord;
		separtor = L"|";
	}

	return GetTagId() + separtor + word;

}