#pragma once
#include "MorphemeInstance.h"
/// <summary>
/// A special type of morpheme that is a single word.
/// Can be made up of many different morphemes.
/// For example dimelo in Spanish has four.
/// A root: Dar-give
/// And three other morphemes: +mandative, to me, it.
/// So dimelo means "give it to me"
/// </summary>
/// <seealso cref="CMorphemeInstance" />
class CMorphemeWord :
	public CMorphemeInstance
{
private:	
	/// <summary>
	/// The root morpheme of the word. 
	/// </summary>
	CMorphemeInstance * mRoot;
		
	/// <summary>
	/// The morphemes that modify the root morpheme.
	/// </summary>
	std::vector<CMorphemeInstance *> mModifiers;
	
	/// <summary>
	/// An intermediate class for the connection between
	/// this word and the morpheme factory that will be
	/// creating the morphemes that make up this word.
	/// </summary>
	class CMorphemeFactoryLink
	{
	private:		
		/// <summary>
		/// The CMorphInstanceFactory we will be getting the 
		/// morphemes from.
		/// </summary>
		CMorphInstanceFactory * mFactory;
	public:
		CMorphemeFactoryLink(CMorphInstanceFactory * factory);
		CMorphemeInstance * MakeMorpheme(std::wstring id);
	};

public:
	CMorphemeWord() = delete;
	CMorphemeWord(std::wstring id, CMorphInstanceFactory * factory);
	virtual ~CMorphemeWord();
};

