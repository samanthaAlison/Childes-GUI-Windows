#include "MorphemeWord.h"
#include "MorphInstanceFactory.h"


/// <summary>
/// Finalizes an instance of the <see cref="CMorphemeWord"/> class.
/// </summary>
/// <param id="id"> The identifying string of the morpheme.</param>
/// <param id="factory"> The factory we will use to create this instance.</param>
CMorphemeWord::CMorphemeWord(std::wstring id, CMorphInstanceFactory * factory) : CMorphemeInstance(id, factory, true)
{
}

/// <summary>
/// Destructor of the <see cref="CMorphemeWord"/> class.
/// </summary>
CMorphemeWord::~CMorphemeWord()
{
}


CMorphemeWord::CMorphemeFactoryLink::CMorphemeFactoryLink(CMorphInstanceFactory * factory)
{
	mFactory = factory;
}

CMorphemeInstance * CMorphemeWord::CMorphemeFactoryLink::MakeMorpheme(std::wstring id)
{
	CMorphemeInstance * morpheme = mFactory->GetMorpheme(id);
	morpheme->IncrementUsageCount();
	return morpheme;

}
