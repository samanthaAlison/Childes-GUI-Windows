#pragma once
#include <TagInstance.h>
#include <TagOption.h>
#include "ChildesContextItem.h"
#include <MorphInstanceFactory.h>
#include "Iterable.h"
class CChildesTagLine;
/// <summary>
/// Class that represents a single word on tag line, as well as all the options
/// for tags for it. 
/// </summary>
/// <seealso cref="CChildesContextItem" />
class CChildesWordUnit :

	public CChildesContextItem, 
#ifdef _STATS


	public CIterable<CTagOption *>
#else
	public CIterable<CTagOptionSimple*>
#endif // _STATS

{
	unsigned int mInfChoice;
	unsigned int mPostChoice;


	/// <summary>
	/// True if this word unit was flagged as invalid;
	/// </summary>
	bool mFlagged = false;

	
	/// <summary>
	/// The dialogue word associated with this unit.
	/// The tags will try to identify its part of speech.
	/// </summary>
	std::wstring mDialogueWord;
	
#ifdef _STATS
	/// <summary>
	/// The list of tags used in this unit. Tags can be used more than once.
	/// </summary>
	std::vector<CTagInstance *> mTags;


	/// <summary>
	/// The morpheme factory where all morphemes for this entire unit will come from. 
	/// </summary>
	CMorphInstanceFactory * mMorphemeFactory;

#endif // _STATS

	
public:
	CChildesWordUnit() = delete;
	CChildesWordUnit(const CChildesWordUnit & other) = delete;
	CChildesWordUnit(std::wstring dialogueWord, std::wstring morTags, std::wstring postTag, CChildesTagLine * parent);
	CChildesWordUnit(std::wstring dialogueWord, std::wstring morTags, std::wstring postTag, std::wstring infTag, CChildesTagLine * parent);
	virtual ~CChildesWordUnit();

	std::wstring GetDialogueWord() { return mDialogueWord; }

	CTagOptionSimple * GetInfChoice();

	std::wstring GetInfChoiceText();

	void SetFlagged(bool flagged) { mFlagged = flagged; }
	bool IsFlagged() { return mFlagged; }

};

