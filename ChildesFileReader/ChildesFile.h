#pragma once
#include "ChildesContextItem.h"
#include <string>
#include <fstream>
#include <iostream>
#include "ChildesTagLine.h"
#include "FileLines.h"




class CChildesFile :
	public CChildesContextItem
{
private:	



	/// <summary>
	/// The name of the file we are reading.
	/// </summary>
	std::wstring mFileName;
	
	/// <summary>
	/// True if the current file has never been opened before.
	/// </summary>
	bool mNewFile;
	

	/// Flag set to true if there is an error reading the file.
	bool mReadingError = false;

	/// <summary>
	/// The lines of this file. 
	/// </summary>
	std::vector<CChildesFileLine *> mFileLines;

	

	void ReadFile(CFileLines & file);
	void ProcessTagLineSection(std::wstring line, CFileLines & file, bool processed);

	void ProcessLine(std::wstring line);

	void LineReadError(std::wstring line, CFileLines & file);
protected:
	virtual void AddLine(CChildesFileLine * line);
	virtual void AddLine(CChildesTagLine * tagLine);
	virtual void AddLine(std::map<TagLineSections::Section, std::wstring>& sections, bool processed);
public:
	CChildesFile() = delete;
	CChildesFile(const CChildesFile &) = delete;
	CChildesFile(CChildesReaderContext * context);
	virtual ~CChildesFile();



	void OpenFile(std::wstring fileName);
	void SaveFile(const wchar_t * fileName);
	
	/// <summary>
	/// Determines whether [is new file]. This means this is the first time
	/// the file has been opened by this program.
	/// </summary>
	/// <returns>
	///   <c>true</c> if [this file is a new file]; otherwise, <c>false</c>.
	/// </returns>
	bool IsNewFile() { return mNewFile; }

	
	///
	/// Gets whether there was an error reading the file. 
	bool ReadingError() { return mReadingError; }

	/// <summary>
	/// Gets the name of the file we are working on.
	/// </summary>
	/// <returns>The file name.</returns>
	const std::wstring & GetFileName() { return mFileName; }
};

