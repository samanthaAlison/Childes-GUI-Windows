#pragma once


/// <summary>
/// A class that provides functions to iterate over a linked list of items
/// that it contains. The type should be a pointer, behavior is undefined otherwise.
/// </summary>
template <typename T> class CIterable
{
private:
protected:	
	/// <summary>
	/// Items that an CIterable class
	/// can iterate over.
	/// </summary>
	class IterableItem
	{
	private:
		/// <summary>
		/// The item that this makes iterable. 
		/// </summary>
		T mItem;

	public:
		/// <summary>
		/// The next iterable item in the list.
		/// </summary>
		IterableItem * mNext = nullptr;
		
		/// <summary>
		/// The previous iterable item in the list.
		/// </summary>
		IterableItem * mPrevious = nullptr;
		
	
		
		/// <summary>
		/// Gets the object this iterable item contains.
		/// </summary>
		/// <returns>The object.</returns>
		T  GetItem() { return mItem; }

		IterableItem(T  item);

		~IterableItem();

	};
private:	
	/// <summary>
	/// The head of the list of iterable items.
	/// </summary>
	IterableItem * mHead = nullptr;

	/// <summary>
	/// The last item in the list of iterable items.
	/// </summary>
	IterableItem * mTail = nullptr;
	
	/// <summary>
	/// The size of the list of iterable items.
	/// </summary>
	size_t mSize = 0;

protected:	
	/// <summary>
	/// Gets the head of the list.
	/// </summary>
	/// <returns>The first item in the list.</returns>
	IterableItem * GetHead() { return mHead; }
	
	/// <summary>
	/// Deletes the pointers.
	/// </summary>
/// <summary>
/// Deletes the pointers.
/// </summary>
	void DeletePointers();

	/// <summary>
	/// Adds the item.
	/// </summary>
	/// <param name="item">The item.</param>
	void AddItem(T item);
public:

	/// <summary>
	/// Iter class that allows for both forward and backwards
	/// iteration of a list of iterable items.
	/// </summary>
	class Iter
	{
	private:
		/// <summary>
		/// The item at this Iter position.
		/// </summary>
		IterableItem * mItem;
		
		/// <summary>
		/// The CIterable object we are iterating through.
		/// </summary>
		CIterable * mIterable;
		
		/// <summary>
		/// The position we are at in the list.
		/// </summary>
		int mPos;
	
	public:
		Iter() = delete;
		Iter(CIterable * iterable, int pos);

		~Iter();


		const bool operator!=(const Iter &other)
		{
			return (mPos != other.mPos);
		}
		
		T operator*() { return mItem->GetItem(); }

		const Iter& operator++();

		const Iter& operator--();

	};


	CIterable();
	virtual ~CIterable();
	
	/// <summary>
	/// Gets the size of the list of items.
	/// </summary>
	/// <returns>The size of the list</returns>
	size_t GetSize() { return mSize; }


	/// <summary>
	/// Gets the Iter to the first item in the list.
	/// </summary>
	/// <returns>The Iter pointing to the start of the list.</returns>
	Iter begin() { return Iter(this, 0); }
	
	/// <summary>
	/// Gets the last iterator one can reach when traveling forward
	/// </summary>
	/// <returns>The iterator at the position equal to the size of the list (the end)</returns>
	Iter end() { return Iter(this, mSize); }
	
	/// <summary>
	/// Gets the first iterator when traveling backwards
	/// </summary>
	/// <returns>The iterator at that points to the tail</returns>
	Iter rbegin() { return Iter(this, mSize - 1); }
	
	/// <summary>
	/// Gets the end iterator when traveling backwards.
	/// </summary>
	/// <returns>The iterator at position -1</returns>
	Iter rend() { return Iter(this, -1); }
	
};



/// <summary>
/// Initializes a new instance of the <see cref="CIterable"/> class.
/// </summary>
template <typename T>
inline CIterable<T>::CIterable()
{
}


/// <summary>
/// Finalizes an instance of the <see cref="CIterable"/> class.
/// </summary>
template <typename T>
inline CIterable<T>::~CIterable()
{
	delete mHead;
}




/// <summary>
/// Initializes a new instance of the <see cref="CIterable"/> class.
/// </summary>
/// <param name="item">The item that this iterable item will point to.</param>
template<typename T>
inline CIterable<T>::IterableItem::IterableItem(T item)
{
	mItem = item;
}

/// <summary>
/// Finalizes an instance of the <see cref="CIterable"/> class.
/// </summary>
template<typename T>
inline CIterable<T>::IterableItem::~IterableItem()
{
	if (this->mNext != nullptr)
	{
		delete this->mNext;
		mNext = nullptr;
	}
	
}


/// <summary>
/// Initializes a new instance of the <see cref="CIterable"/> class.
/// </summary>
/// <param name="iterable">The iterable object we are iterating over.</param>
/// <param name="pos">The position of this iterator in the list.</param>
template<typename T>
inline CIterable<T>::Iter::Iter(CIterable * iterable, int pos)
{
	mIterable = iterable;
	mPos = pos;
	// CIterable::rend() or CIterable::end()
	if (mPos == -1 || mPos == iterable->mSize)
	{
		mItem = nullptr;
	}
	// CIterable::rbegin()
	else if (mPos == iterable->mSize - 1) {
		mItem = iterable->mTail;
	}
	else
	{
		if (mPos >= (int) iterable->mSize / 2)
		{
			mItem = iterable->mTail;
			int i = iterable->mSize - 1;
			for (; i > mPos; i--)
			{
				mItem = mItem->mPrevious;
			}
		}
		else
		{
			mItem = iterable->mHead;
			int i = 0;
			for (; i < mPos; i++)
			{
				mItem = mItem->mNext;
			}
			
		}
	}

}
/// <summary>
/// Destructor.
/// </summary>
template<typename T>
inline CIterable<T>::Iter::~Iter()
{
}

/// <summary>
/// Get the next iterator.
/// </sumary>
/// <returns>The next iterator.</returns>
template<typename T>
inline const typename CIterable<T>::Iter & CIterable<T>::Iter::operator++()
{
	mPos++;
	if (*this != mIterable->end())
		mItem = mItem->mNext;
	return *this;
	
}

/// <summary>
/// Get the previous iterator.
/// </sumary>
/// <returns>The previous iterator.</returns>
template<typename T>
inline const typename CIterable<T>::Iter & CIterable<T>::Iter::operator--()
{
	mPos--;
	if (*this != mIterable->rend())
		mItem = mItem->mPrevious;
	return *this;

}

/// <summary>
/// DO NOT USE IF YOUR TEMPLATED TYPE IS NOT A POINTER.
/// Delete the composited items contained in the IterableItem list
/// if they are pointers.
/// </summary>
template<typename T>
inline void CIterable<T>::DeletePointers()
{
	IterableItem * iItem = mHead;
	while (iItem != nullptr)
	{
		delete iItem->GetItem();
		iItem = iItem->mNext;
	}

}

/// <summary>
/// Add an item to the list.
/// </sumary>
/// <param name ="item"> The item to be added.</param>
template<typename T>
inline void CIterable<T>::AddItem(T item)
{
	IterableItem * iItem = new IterableItem(item);

	// First item of list.
	if (mHead == nullptr)
	{
		mHead = iItem;
		mTail = iItem;
	}
	else
	{
		mTail->mNext = iItem;
		iItem->mPrevious = mTail;
		mTail = iItem;
	}
	iItem->mNext = nullptr;
	mSize++;
}