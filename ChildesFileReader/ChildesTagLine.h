#pragma once
#include "ChildesFileLine.h"
#include <string>
#include <vector>
#include <map>
#include "ChildesWordUnit.h"
#include "Iterable.h"
namespace TagLineSections
{
	const int PREFIX_LENGTH(4);

	/// <summary>
	/// The different sections of the Tag line text 
	/// </summary>
	enum Section {
		/// <summary>
		/// All of the text.
		/// </summary>
		CONST,
		/// <summary>
		/// The dialogue line.
		/// </summary>
		DIALOGUE,
		/// <summary>
		/// The MOR line.
		/// </summary>
		MOR,
		/// <summary>
		/// The POST line.
		/// </summary>
		POST,
		/// <summary>
		/// The comment line.
		/// </summary>
		COM,
		/// <summary>
		/// The informant line.
		/// </summary>
		INF
	};
	
	/// <summary>
	/// The mor section prefix
	/// </summary>
	const std::wstring MOR_PREFIX(L"%mor");	
	/// <summary>
	/// The post section prefix
	/// </summary>
	const std::wstring POST_PREFIX(L"%pst");	
	/// <summary>
	/// The comment section prefix
	/// </summary>
	const std::wstring COMMENT_PREFIX(L"%com");	
	/// <summary>
	/// The informant section prefix
	/// </summary>
	const std::wstring INFORMANT_PREFIX(L"%inf");

	Section SectionFromPrefix(std::wstring prefix);

};

/// <summary>
/// A Childes File line that contains dialogue of the conversation the file is 
/// a transcript of, as well as tagging information. 
/// </summary>
/// <seealso cref="CChildesFileLine" />
class CChildesTagLine :
	public CChildesFileLine, public CIterable<CChildesWordUnit *>
{
protected:
	
private:	
	
	
	/// <summary>
	/// Flag that is true if this line has been processed by the informant.
	/// </summary>
	bool mLineProcessed = false;
	
	/// <summary>
	/// Represents a comment line for this tagging line (if one exists).
	/// </summary>
	std::wstring mCommentLine = L"";
	

	std::vector<std::wstring> SplitSection(const std::wstring &section);
	void ProcessDialogue(std::vector<std::wstring> &dialogueWords);
		
	/// <summary>
	/// Used to keep track of ending punctuation in the %inf line.
	/// </summary>
	std::wstring mLastPostWord;

	
	
protected:
	
	virtual std::wstring GetFullLine() override;
	CChildesTagLine(CChildesTagLine * tagLine, bool deleteHere = false);
public:
	CChildesTagLine() = delete;
	CChildesTagLine(const CChildesTagLine & tagLine);
	CChildesTagLine(std::map<TagLineSections::Section, std::wstring> & sections, CChildesFile * parent);
	virtual ~CChildesTagLine();	

	std::wstring GetDialogueLine();

	
	/// <summary>
	/// Determines whether this line has processed by the informant.
	/// </summary>
	/// <returns>
	///   <c>true</c> if this line has been processed; otherwise, <c>false</c>.
	/// </returns>
	bool IsProcessed() { return mLineProcessed; }
	
	/// <summary>
	/// Sets the line processed flag.
	/// </summary>
	/// <param name="processed">if set to <c>true</c> [this line has processed by the informant].</param>
	void SetProcessed(bool processed) { mLineProcessed = processed; }
		
	bool IsFlagged();


};

