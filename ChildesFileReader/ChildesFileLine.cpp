#include "ChildesFileLine.h"
#include "ChildesFile.h"



/// <summary>
/// Initializes a new instance of the <see cref="CChildesFileLine"/> class.
/// </summary>
/// <param name="lineText">The line text.</param>
/// <param name="parent">The parent item that created this.</param>
CChildesFileLine::CChildesFileLine(std::wstring lineText, CChildesFile * parent) : CChildesContextItem(parent)
{

	mLineText = lineText;
}

/// <summary>
/// Copy constructor of the <see cref="CChildesFileLine"/> class.
/// </summary>
/// <param name="line">The file line we are copying.</param>
CChildesFileLine::CChildesFileLine(const CChildesFileLine & line) : CChildesContextItem(line)
{
	mLineText = line.mLineText;
}

/// <summary>
/// Finalizes an instance of the <see cref="CChildesFileLine"/> class.
/// </summary>
CChildesFileLine::~CChildesFileLine()
{
}
