#include "ChildesWordUnit.h"
#include "ChildesTagLine.h"
#include <sstream>



/// <summary>
/// Initializes a new instance of the <see cref="CChildesWordUnit"/> class.
/// </summary>
/// <param name="dialogueWord">The dialogue word.</param>
/// <param name="morTags">The mor tags.</param>
/// <param name="postTag">The post tag.</param>
/// <param name="parent">The parent.</param>
CChildesWordUnit::CChildesWordUnit(std::wstring dialogueWord, std::wstring morTags, std::wstring postTag, CChildesTagLine * parent) : CChildesWordUnit(dialogueWord, morTags, postTag, L"", parent)
{
	
	
}

/// <summary>
/// Initializes a new instance of the <see cref="CChildesWordUnit"/> class.
/// </summary>
/// <param name="dialogueWord">The dialogue word.</param>
/// <param name="morTags">The mor tags.</param>
/// <param name="postTag">The post tag.</param>
/// <param name="infTag">The inf tag.</param>
/// <param name="parent">The parent.</param>
CChildesWordUnit::CChildesWordUnit(std::wstring dialogueWord, std::wstring morTags, std::wstring postTag, std::wstring infTag, CChildesTagLine * parent) : CChildesContextItem(parent)
{
	mDialogueWord = dialogueWord;

	std::wistringstream wss(morTags);
	std::wstring tag;

	mFlagged = (infTag.find(L"?|") != std::wstring::npos);

	while (std::getline(wss, tag, L'^'))
	{
		std::wstring tagString = tag.substr(0, tag.find(L"|"));

		

		if (postTag == tag)
		{
			mPostChoice = GetSize();
			if (infTag.empty() || mFlagged)
			{
				mInfChoice = mPostChoice;
			}
		}
		if (!infTag.empty() || mFlagged)
		{
			if (infTag == tag)
			{
				mInfChoice = this->GetSize();
			}
		}
#ifdef _STATS

		CTagInstance * instance = new CTagInstance(tagString, GetContext()->GetTagManager(), mMorphemeFactory);
		mTags.push_back(instance);
		AddItem(new CTagOption(tag.substr(tag.find(L"|") + 1), instance, mMorphemeFactory));

#else
	
		AddItem(new CTagOptionSimple(tagString, tag.substr(tag.find(L"|") + 1)));
#endif
	}

	CChildesWordUnit::Iter postIter = Iter(this, mPostChoice);
	CChildesWordUnit::Iter infIter = Iter(this, mInfChoice);
	(*postIter)->SetPostChoice(true);
	(*infIter)->SetInfChoice(true);
}


/// <summary>
/// Finalizes an instance of the <see cref="CChildesWordUnit"/> class.
/// </summary>
CChildesWordUnit::~CChildesWordUnit()
{
	DeletePointers();

}

CTagOptionSimple * CChildesWordUnit::GetInfChoice()
{
	for (CChildesWordUnit::Iter iter = begin(); iter != end(); ++iter)
	{
		if ((*iter)->IsInfChoice())
		{
			return *iter;
		}
	}
	return nullptr;
}

/// <summary>
/// Gets the text for the tag the informant chose, for use in saving and outputing. 
/// If this unit is flagged, it will instead return the propper tag for the dialogue word
/// being undefined.
/// </summary>
/// <returns></returns>
std::wstring CChildesWordUnit::GetInfChoiceText()
{
	if (mFlagged)
	{
		return L"?|" + mDialogueWord;
	}
	return GetInfChoice()->GetFullTag();
}
