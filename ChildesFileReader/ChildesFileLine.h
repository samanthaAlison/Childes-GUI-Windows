#pragma once
#include "ChildesContextItem.h"
#include <string>
class CChildesFile;
class CChildesFileLine :
	public CChildesContextItem
{
private:	
	/// <summary>
	/// The line's text
	/// </summary>
	std::wstring mLineText;
	
	/// <summary>
	/// The file this is a line of.
	/// </summary>
	CChildesFile * mFile;

protected:	
	/// <summary>
	/// Gets the file that this is a line of.
	/// </summary>
	/// <returns>The file that owns this line.</returns>
	CChildesFile * GetFile() { return mFile; }

	
	/// <summary>
	/// Gets the full line for use in writing the file.
	/// </summary>
	/// <returns></returns>
	virtual std::wstring GetFullLine() { return mLineText; }
public:
	CChildesFileLine(std::wstring lineText, CChildesFile * parent);
	CChildesFileLine(const CChildesFileLine & line);
	virtual ~CChildesFileLine();
	
	/// <summary>
	/// Gets the line's text.
	/// </summary>
	/// <returns>The line's text</returns>
	std::wstring GetLineText() { return mLineText; }

	friend std::wostream & operator<<(std::wostream & out, CChildesFileLine & line)
	{
		out << line.GetFullLine();
		return out;
	}

	
};

