#include "ChildesContextItem.h"



/// <summary>
/// Initializes a new instance of the <see cref="CChildesContextItem"/> class.
/// </summary>
/// <param name="parent">The parent of this item.</param>
CChildesContextItem::CChildesContextItem(CChildesContextItem * parent)
{
	mContext = parent->GetContext();
}

/// <summary>
/// Copy constructor of the <see cref="CChildesContextItem"/> class.
/// </summary>
/// <param name="item">The item we are copying.</param>
CChildesContextItem::CChildesContextItem(const CChildesContextItem & item)
{
	mContext = item.mContext;
}


/// <summary>
/// Initializes a new instance of the <see cref="CChildesContextItem"/> class.
/// </summary>
/// <param name="context">The context.</param>
CChildesContextItem::CChildesContextItem(CChildesReaderContext * context)
{
	mContext = context;
}


/// <summary>
/// Destructor of the <see cref="CChildesContextItem"/> class.
/// </summary>
CChildesContextItem::~CChildesContextItem()
{

}
