#pragma once
#define _CRTDBG_MAP_ALLOC
#ifdef _DEBUG
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
// Replace _NORMAL_BLOCK with _CLIENT_BLOCK if you want the
// allocations to be of _CLIENT_BLOCK type
#else
#define DBG_NEW new
#endif
#include "ChildesFile.h"
#include "ChildesTagLine.h"
#include "Iterable.h"


/// <summary>
/// A subclass of CChildesFile that specializes in iterating over 
/// the tag lines.
/// </summary>
/// <seealso cref="CChildesFile" />
class CChildesTagLineBrowser :
	public CChildesFile, public CIterable<CChildesTagLine *>
{
protected:	

	


private:
	virtual void AddLine(std::map<TagLineSections::Section, std::wstring> & sections, bool processed);

public:	
	/// <summary>
	/// Gets the tag line count.
	/// </summary>
	/// <returns>The tag line count.</returns>
	int GetTagLineCount() { return GetSize(); }
	

	CChildesTagLineBrowser(CChildesReaderContext * context);
	virtual ~CChildesTagLineBrowser();
	
	
};

