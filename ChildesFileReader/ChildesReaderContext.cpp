#include "ChildesReaderContext.h"
#include <string>
#include <vector>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <locale>

const std::wstring  DIALOGUE_START_BLACK_LIST(L"DIALOGUE_START_BLACK_LIST");
const std::wstring  MORTAG_START_BLACK_LIST(L"MORTAG_START_BLACK_LIST");
const std::wstring  MORTAG_BLACK_LIST(L"MORTAG_BLACK_LIST");
const std::wstring  DIALOGUE_BLACK_LIST(L"DIALOGUE_BLACK_LIST");

 std::vector<std::wstring> CChildesReaderContext::FIELDS = {
	L"MOR Usage Count",
	L"POST Usage Count",
	L"POST Wrong Count",
	L"Inf Usage Count",
	L"Inf Flag Count"
};




/// <summary>
/// Initializes a new instance of the <see cref="CChildesReaderContext"/> class.
/// </summary>
 CChildesReaderContext::CChildesReaderContext(std::wstring blacklistFileName) : mBlackList(blacklistFileName)
{

	mBlacklistFileName = blacklistFileName;
	std::vector<std::wstring> fieldNames = std::vector<std::wstring>(std::begin(FIELDS), std::end(FIELDS));

	mTagManager = new CStatsItemManager(fieldNames);
	
	mMorphemeManager = new CStatsItemManager(fieldNames);

	

}


/// <summary>
/// Finalizes an instance of the <see cref="CChildesReaderContext"/> class.
/// </summary>
CChildesReaderContext::~CChildesReaderContext()
{
	delete mTagManager;
	delete mMorphemeManager;
}

/// <summary>
/// Gets the text for one of the stats item fields.
/// </summary>
/// <param name="fieldName">The enum value that represents the field.</param>
/// <returns></returns>
std::wstring CChildesReaderContext::GetFieldText(FieldName fieldName)
{
	switch (fieldName)
	{
	case FieldName::INF_FLAG_COUNT:
		return FIELDS[4];
	case FieldName::INF_USAGE_COUNT:
		return FIELDS[3];
	case FieldName::MOR_USAGE_COUNT:
		return FIELDS[0];
	case FieldName::POST_USAGE_COUNT:
		return FIELDS[1];
	case FieldName::POST_WRONG_COUNT:
		return FIELDS[2];
	default:
		return std::wstring();
	};

}

bool CChildesReaderContext::FilterDialogue(std::wstring word)
{
	
	return mBlackList.DialogueBlackList(word);
	
}

bool CChildesReaderContext::FilterMor(std::wstring word)
{
	return mBlackList.MorBlackList(word);
}


/// <summary>
/// Adds a message to the debug log.
/// </summary>
/// <param name="msg">The message to be added.</param>
void CChildesReaderContext::AppendDebugLog(std::wstring msg)
{

	time_t _t = std::time(nullptr);
	wchar_t wstr[100];
	struct tm _time_info;
	

	localtime_s(&_time_info, &_t);
	if (std::wcsftime(wstr, 100, L"%d-%m-%Y %H:%M:%S", &_time_info)) {

		mDebugLog << L"[" << wstr << L"]\t";
	}
	mDebugLog << msg << std::endl;



}

/// <summary>
/// Initializes a new instance of the <see cref="WordBlackList"/> class.
/// </summary>
CChildesReaderContext::WordBlackList::WordBlackList(std::wstring fileName)
{

	std::wifstream blackListFile(fileName);
	
	std::wstring listName = DIALOGUE_START_BLACK_LIST;

	std::wstring line;
	while (std::getline(blackListFile, line))
	{
		if (line == DIALOGUE_START_BLACK_LIST ||
			line == DIALOGUE_BLACK_LIST || 
			line == MORTAG_BLACK_LIST ||
			line == MORTAG_START_BLACK_LIST)
		{
			listName = line;
		}
		else if (line == L"")
		{
			// Do nothing; this line is empty.
		}
		else
		{
			if (listName == DIALOGUE_START_BLACK_LIST)
			{
				mDialogueStartBlackList.insert(line[0]);
			}
			else if (listName == DIALOGUE_BLACK_LIST)
			{
				mDialogueWordBlackList.insert(line);
			}
			else if (listName == MORTAG_BLACK_LIST)
			{
				mMorWordBlackList.insert(line);
			}
			else if (listName == MORTAG_START_BLACK_LIST)
			{
				mMorStartBlackList.insert(line[0]);
			}
		}
	}

}

/// <summary>
/// Finalizes an instance of the <see cref="WordBlackList"/> class.
/// </summary>
CChildesReaderContext::WordBlackList::~WordBlackList()
{
}

bool CChildesReaderContext::WordBlackList::MorBlackList(std::wstring word)
{
	wchar_t head = word[0];
	if (mMorStartBlackList.find(head) != mMorStartBlackList.end())
	{
		return true;
	}
	else if (mMorWordBlackList.find(word) != mMorWordBlackList.end())
	{
		return true;
	}
	return false;
}

bool CChildesReaderContext::WordBlackList::DialogueBlackList(std::wstring word)
{

	wchar_t head = word[0];
	if (mDialogueStartBlackList.find(head) != mDialogueStartBlackList.end())
	{
		return true;
	}
	else if (mDialogueWordBlackList.find(word) != mDialogueWordBlackList.end())
	{
		return true;
	}
	return false;
}
