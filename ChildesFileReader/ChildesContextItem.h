#pragma once
#include "ChildesReaderContext.h"
/// <summary>
/// An item that can access the  <see cref="CChildesReaderContext"/>.
/// </summary>
class CChildesContextItem
{
private:	
	/// <summary>
	/// The context this item will be using.
	/// </summary>
	CChildesReaderContext * mContext;

protected:
	
	/// <summary>
	/// Gets the ChildesReaderContext of this item.
	/// </summary>
	/// <returns>The context</returns>
	CChildesReaderContext * GetContext() { return mContext; }
	CChildesContextItem(CChildesContextItem * parent);
	CChildesContextItem(const CChildesContextItem & item);
	/// <summary>
	/// Adds a message to the context's debug log.
	/// </summary>
	/// <param name="msg">The message to be added.</param>
	void Log(std::wstring msg) { mContext->AppendDebugLog(msg); }

public:
	CChildesContextItem()=delete;
	CChildesContextItem(CChildesReaderContext * context);
	virtual ~CChildesContextItem();


};

