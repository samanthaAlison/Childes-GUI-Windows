
#pragma once
#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC 
#include <crtdbg.h>
#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW
#endif
#include <StatsItemManager.h>
#include <vector>
#include <string>
#include <unordered_set>
#include <sstream>
#include <iterator>
#include <algorithm>

/// <summary>
/// Context to be created when reading Childes Files.
/// Only one context should exist at a time. 
/// </summary>
class CChildesReaderContext
{
private:


	
	static  std::vector<std::wstring> FIELDS;

	
	/// <summary>
	/// The debugging log.
	/// </summary>
	std::wostringstream mDebugLog;



	/// <summary>
	/// The StatsItem manager for the tags. All the tags in this context will
	/// come from this manager
	/// </summary>
	CStatsItemManager * mTagManager;

	/// <summary>
	/// The StatsItem manager for the morphemes. All the morphemes in this context
	/// will come from this manager.
	/// </summary>
	CStatsItemManager * mMorphemeManager;


	class WordBlackList
	{
	private:
		/// <summary>
		/// List of characters such that
		/// words in MOR lines that start with these characters
		/// will be ignored.
		/// </summary>
		std::unordered_set<wchar_t> mMorStartBlackList;		
		/// <summary>
		/// List of characters such that
		/// words in dialogue lines that start with these characters
		/// will be ignored.
		/// </summary>
		std::unordered_set<wchar_t> mDialogueStartBlackList;
				
		/// <summary>
		/// List of words to be ignored in the MOR lines.
		/// </summary>
		std::unordered_set<std::wstring> mMorWordBlackList;
				
		/// <summary>
		/// List of words to be ignored in the dialogue lines.
		/// </summary>
		std::unordered_set<std::wstring> mDialogueWordBlackList;

	public:
		WordBlackList(std::wstring fileName);
		~WordBlackList();
		
		bool MorBlackList(std::wstring word);
		bool DialogueBlackList(std::wstring word);


	};
	
	/// <summary>
	/// The black listed words for our files.
	/// </summary>
	WordBlackList mBlackList;


	std::wstring mBlacklistFileName;
public:




	CChildesReaderContext(std::wstring blacklistFileName = L"./FileWordBlackList.txt");
	~CChildesReaderContext();
	
	/// <summary>
	/// Gets the morpheme manager for this context.
	/// </summary>
	/// <returns>The morpheme manager</returns>
	CStatsItemManager * GetMorphemeManager() { return mMorphemeManager; }	
	/// <summary>
	/// Gets the tag manager for this context.
	/// </summary>
	/// <returns>The tag manager</returns>
	CStatsItemManager * GetTagManager() { return mTagManager; }

	
	enum FieldName {
		MOR_USAGE_COUNT,
		POST_USAGE_COUNT,
		POST_WRONG_COUNT,
		INF_USAGE_COUNT,
		INF_FLAG_COUNT
	};

	std::wstring GetFieldText(FieldName fieldName);

	bool FilterDialogue(std::wstring word);
	bool FilterMor(std::wstring word);


	void AppendDebugLog(std::wstring msg);

	
	/// <summary>
	/// Gets the debug log contents.
	/// </summary>
	/// <returns>The debug log contents</returns>
	std::wstring GetDebugLog() { return mDebugLog.str(); }
	
	
	/// <summary>
	/// Clears the debug log.
	/// </summary>
	void ClearDebugLog() { mDebugLog.str(L""); }
};

