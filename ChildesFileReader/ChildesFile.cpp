#include "ChildesFile.h"
#include "ChildesFileLine.h"
#include "ChildesTagLine.h"
#include <stdexcept>
using TagLineSections::Section;
#include <map>

//using std::getline;

const std::wstring NEW_FILE_EXTENSION(L".inprep");
const std::wstring FILE_SAVE_EXTENSION(L".out");




/// <summary>
/// Reads the file and processes it.
/// </summary>
/// <param name="file">The file we are reading.</param>
 void CChildesFile::ReadFile(CFileLines & file)
{
	bool tagLineProcessed = false;
	
	std::wstring line;

	while (file.getline( line))
	{
		// We are dealing with a processed tagging line.
		if ((line.compare(L"@processed") == 0))
		{
			tagLineProcessed = true;
		}
		// We are dealing with a tagging line.
		else if (line[0] == L'*')
		{
			ProcessTagLineSection(line, file, tagLineProcessed);
			tagLineProcessed = false;

		}		
		// Ignore this line completely, lest it be duplicated every time the file is saved.
		// It is designed to notify people reading the file that a line had word units the 
		// informant believed to be invalid. 
		else if (line.compare(L"@flagged") == 0)
		{

		}
		


		// We are dealing with a metadata line, which will not have tags or any tag
		// lines associated with it.
		else if (line[0] == L'@')
		{
			AddLine(new CChildesFileLine(line + L"\n", this));
		}
		
	}

}

/// <summary>
/// Processes the tag line sections and creates a CChildesTagLine.
/// </summary>
/// <param name="line">The first line of the TagLine.</param>
/// <param name="file">The file we are reading from.</param>
/// <param name="processed">Flag set to true if the line has been processed.</param>
void CChildesFile::ProcessTagLineSection(std::wstring line, CFileLines & file, bool processed)
{
	std::map<Section, std::wstring> sections;
	sections[Section::CONST] = line + L"\n";
	// Get the dialogue line sans the speaker_id/prefix.
	sections[Section::DIALOGUE] = line.substr(line.find(L":\t") + 2);
	std::wstring nextLine = L"";
	int lineCount = 1;
	while (file.getline(nextLine) && (nextLine != L"")
		&& !(nextLine[0] == L'*' || nextLine[0] == L'@'))
	{
		lineCount++;



		std::wstring prefix = nextLine.substr(0, TagLineSections::PREFIX_LENGTH);

		Section sect = TagLineSections::SectionFromPrefix(prefix);
	

		if (sect == Section::CONST || sect == Section::MOR || sect == Section::POST)
		{
			sections[Section::CONST] += nextLine + L"\n";
		}

		if (TagLineSections::PREFIX_LENGTH + 1 < nextLine.size())
		{
			sections[sect] = nextLine.substr(TagLineSections::PREFIX_LENGTH + 1);
		}
	
	}

	file.seek_back();

	if ( nextLine == L"" || sections.find(Section::MOR) == sections.end())
	{
		CChildesFileLine * fileLine = new CChildesFileLine(sections[Section::CONST], this);

		AddLine(fileLine);

		LineReadError(line, file);

	}
	else
	{
		try {
			AddLine(sections, processed);
		}
		catch  (std::exception& exception){
			CChildesFileLine * fileLine = new CChildesFileLine(sections[Section::CONST], this);

			AddLine(fileLine);

			LineReadError(line, file);

		}
	}
	
}

void CChildesFile::ProcessLine(std::wstring line)
{
}


/// <summary>
/// Dumps an error that occured at a particular line.
/// </summary>
/// <param name="line">The line.</param>
void CChildesFile::LineReadError(std::wstring line, CFileLines & file)
{
	mReadingError = true;

	std::wostringstream logMsg = std::wostringstream(L"");

	logMsg << L"Error on line number" << file.line_number() << ": " << line << L"\n";

	this->Log(logMsg.str());
}

void CChildesFile::AddLine(CChildesFileLine * line)
{
	mFileLines.push_back(line);
}

void CChildesFile::AddLine(CChildesTagLine * tagLine)
{
	mFileLines.push_back(tagLine);
}

void CChildesFile::AddLine(std::map<TagLineSections::Section, std::wstring>& sections , bool processed)
{
	CChildesTagLine  * tagLine = new CChildesTagLine(sections, this);
	tagLine->SetProcessed(processed);
	AddLine(tagLine);

}


/// <summary>
/// Initializes a new instance of the <see cref="CChildesFile"/> class.
/// </summary>
/// <param name="context">The context we will be using for this file.</param>
CChildesFile::CChildesFile(CChildesReaderContext * context) : CChildesContextItem(context)
{
	
}


/// <summary>
/// Finalizes an instance of the <see cref="CChildesFile"/> class.
/// </summary>
CChildesFile::~CChildesFile()
{
	// Delete all of the lines.
	for (std::vector<CChildesFileLine *>::iterator iter = mFileLines.begin();
		iter != mFileLines.end(); ++iter)
	{
		delete (*iter);
	}

}

/// <summary>
/// Opens the childes file.
/// </summary>
/// <param name="fileName">Name of the file.</param>
void CChildesFile::OpenFile(std::wstring fileName)
{

	mReadingError = false;

	


	//
	// Determine if the file we are opening is new or not.
	//

	// Firstly, we need to determine if it has the new file extension or 	
	// the file extension this program uses when it saves the file.
	int extPos = (int)fileName.find_last_of(L".");
	std::wstring ext = fileName.substr(extPos);
	if (ext.compare(NEW_FILE_EXTENSION) == 0)
	{
		// The filename has the unprocessed (new) extension.
		// Let's make sure there isn't a processed version in
		// this directory.
		if (!(std::wifstream(fileName + FILE_SAVE_EXTENSION)))
		{
			// The is not a processed version. This truly is a file that has 
			// not been processed.
			mNewFile = true;	
		}
		else
		{
			// A processed version exists. Open that instead.
			mNewFile = false;
			fileName += FILE_SAVE_EXTENSION;
		}
	}
	else if (ext.compare(FILE_SAVE_EXTENSION) == 0)
	{
		// This file has been processed (it is not new).
		mNewFile = false;
	}


	this->Log(L"Attempting to read file at " + fileName);

	mFileName = fileName;
	std::wifstream file(fileName.c_str());


	CFileLines fileLines(file);

	ReadFile(fileLines);

}


/// <summary>
/// Saves the file.
/// </summary>
/// <param name="fileName">Name to save the file as.</param>
void CChildesFile::SaveFile(const wchar_t * fileName)
{
	std::wofstream saveFile(fileName);
	for (CChildesFileLine * line : mFileLines)
	{
		saveFile << *line;
	}
	saveFile.close();
}
