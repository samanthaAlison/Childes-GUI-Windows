#include "ChildesTagLine.h"
#include "ChildesFile.h"
#include "ChildesWordUnit.h"
#include <iostream>
#include <sstream>
#include <exception>
const std::wstring PREFIX_ENDING(L":\t");
const std::wstring PROCESSED_TAG(L"@processed\n");
const std::wstring FLAGGED_TAG(L"@flagged\n");
using namespace TagLineSections;

/// <summary>
/// Splits the section up into individual words.
/// </summary>
/// <param name="section">The section we are splitting.</param>
/// <returns> A vector containing each word as an entry.</returns>
std::vector<std::wstring> CChildesTagLine::SplitSection(const std::wstring & section)
{
	std::wstringstream wss(section);
	std::istream_iterator<std::wstring, wchar_t> begin(wss);
	std::istream_iterator<std::wstring, wchar_t> end;
	std::vector<std::wstring> words(begin, end);

	return words;
}

/// <summary>
/// Processes the dialogue words, remove invalid ones, and format them when needed.
/// </summary>
/// <param name="dialogueWords">The dialogue words.</param>
void CChildesTagLine::ProcessDialogue(std::vector<std::wstring>& dialogueWords)
{
	CChildesReaderContext * context = GetContext();

	



	for (int i = 0; i < dialogueWords.size(); i++) 
	{
		// Special case where one word means another word or multiple words. 
		if (dialogueWords.at(i).find(L"[:") != std::wstring::npos) 
		{
			if (i > 0) 
			{
				// Turn them into blacklisted items so the blacklist removes them.
				dialogueWords.at(i - 1) = L",";
				dialogueWords.at(i) = L",";

				while (dialogueWords.at(i).find(L"]") == std::wstring::npos) 
				{
					i++;
				}

				dialogueWords.at(i) = dialogueWords.at(i).substr(0, dialogueWords.at(i).size() - 1);

			}
		} 
		
	}

	dialogueWords.erase(
		std::remove_if(dialogueWords.begin(),
			dialogueWords.end(),
			[context](std::wstring word) { return context->FilterDialogue(word); }),
		dialogueWords.end());



}

/// <summary>
/// Gets the dialogue line of this tag line.
/// </summary>
/// <returns>The dialogue line.</returns>
std::wstring CChildesTagLine::GetDialogueLine()
{
	std::wstring lineText = GetLineText();
	int pos = lineText.find(L"\n");
	return lineText.substr(0, pos - 1);
}




/// <summary>
/// Gets the full line that will be written to the file.
/// </summary>
/// <returns>The full line, as a string.</returns>
std::wstring CChildesTagLine::GetFullLine()
{
	std::wostringstream text;
	if (mLineProcessed)
	{
		text << PROCESSED_TAG;
		if (IsFlagged())
		{
			text << FLAGGED_TAG;
		}
	}
	text << GetLineText();
	text << INFORMANT_PREFIX << L":\t";

	if (begin() != end())
	{
		CChildesTagLine::Iter iter = begin();

		text << (*iter)->GetInfChoiceText();

		++iter;
		for (; iter != end(); ++iter)
		{
			text << L" " << (*iter)->GetInfChoiceText();
		}
	}
	if (mLastPostWord != L"")
	{
		text << L" " << mLastPostWord;
	}
	text << L"\n";
	if (!mCommentLine.empty())
	{
		text << COMMENT_PREFIX << L":" << mCommentLine << L"\n";
	}

	return text.str();
}

/// <summary>
/// Changeling constructor of the <see cref="CChildesTagLine"/> class.
/// If you don't know what that means, don't use it, or read the full 
/// documentation first.
/// </summary>
/// <remarks>
/// A changeling constructor is designed to intercept an object being
/// passed to a function (usually to be added to a list), and copy it. 
/// Then it will modify values, derive it, or change it in some other 
/// way, then delete it and take its place. This constructor and its
/// overrides can only cover the copying, modifying, and deleting 
/// aspects of this design pattern. If you use this, it is up to YOU
/// to have your changeling copy to replace the intercepted object,
/// unless you like passing around invalid pointers. 
/// <param name="tagLine">The tag line intercepted.</param>
/// <param name="deleteHere">If set to true, the intercepted object 
/// will be deleted here.</param>
CChildesTagLine::CChildesTagLine(CChildesTagLine * tagLine, bool deleteHere) : CChildesTagLine(*tagLine)
{

}

/// <summary>
/// Copy constructor of the <see cref="CChildesTagLine"/> class.
/// </summary>
/// <param name="tagLine">The tag line we are copying.</param>
CChildesTagLine::CChildesTagLine(const CChildesTagLine & tagLine) : CChildesFileLine(tagLine), CIterable<CChildesWordUnit *>(tagLine)
{

}

/// <summary>
/// Initializes a new instance of the <see cref="CChildesTagLine"/> class.
/// </summary>
/// <param name="sections">The sections of the TagLine, split up.</param>
/// <param name="parent">The parent item of this.	</param>
CChildesTagLine::CChildesTagLine(std::map<TagLineSections::Section, std::wstring> & sections, CChildesFile * parent) : CChildesFileLine(sections[CONST], parent)
{


	mLastPostWord = L"";

	CChildesReaderContext * context = GetContext();
	std::vector<std::wstring> dialogueWords = SplitSection(sections[DIALOGUE]);
	
	ProcessDialogue(dialogueWords);

	std::vector<std::wstring> morWords = SplitSection(sections[MOR]);
	morWords.erase(
		std::remove_if(morWords.begin(),
			morWords.end(),
			[context](std::wstring word) { return context->FilterMor(word); }),
		morWords.end());

	if (morWords.size() != dialogueWords.size()) {
		
		throw std::exception("A tag line in this file is invalid.");
	}
	


	if (sections.find(COM) != sections.end())
	{
		mCommentLine = sections[COM];
	}
	else
	{
		mCommentLine = L"";


	}

	std::vector<std::wstring> postWords = SplitSection(sections[POST]);


	try {
		if (sections.find(INF) != sections.end())
		{
			

			CChildesWordUnit * unit;

			std::vector<std::wstring> infWords = SplitSection(sections[INF]);
			if (postWords.size() - infWords.size() > 0)
			{
				mLastPostWord = postWords.back();
			}
			for (unsigned int i = 0; i < dialogueWords.size(); i++)
			{
				unit = new CChildesWordUnit(dialogueWords.at(i), morWords.at(i), postWords.at(i), infWords.at(i), this);
				AddItem(unit);
			}
		}
		else
		{
			CChildesWordUnit * unit;


			for (unsigned int i = 0; i < dialogueWords.size(); i++)
			{
				unit = new CChildesWordUnit(dialogueWords.at(i), morWords.at(i), postWords.at(i), this);
				AddItem(unit);
			}
		}
	}
	catch (std::out_of_range &e) {
		GetContext()->AppendDebugLog(L"Error: not enough post words");
		throw std::exception("Not enough post words on line");


	}
	
	

	
	/*std::wcout << sections[DIALOGUE] << std::endl
		<< MOR_PREFIX << PREFIX_ENDING << sections[MOR] << std::endl
		<< POST_PREFIX << PREFIX_ENDING << sections[POST] << std::endl
		<< ((sections.find(COM) != sections.end()) ? (COMMENT_PREFIX + PREFIX_ENDING + sections[COM] + L"\n") : (L""))
		<< INFORMANT_PREFIX << PREFIX_ENDING << sections[INF] << std::endl;*/
}



/// <summary>
/// Finalizes an instance of the <see cref="CChildesTagLine"/> class.
/// </summary>
CChildesTagLine::~CChildesTagLine()
{
	DeletePointers();
}

TagLineSections::Section TagLineSections::SectionFromPrefix(std::wstring prefix)
{
	Section section;
	if (prefix == MOR_PREFIX)
	{
		section = MOR;
	}
	else if (prefix == POST_PREFIX)
	{
		section = POST;
	}
	else if (prefix == INFORMANT_PREFIX)
	{
		section = INF;
	}
	else if (prefix == COMMENT_PREFIX)
	{
		section = COM;
	}
	else
	{
		section = CONST;
	}
	return section;
}


/// <summary>
/// Determines whether this tag line has any flagged word units.
/// </summary>
/// <returns>
///   <c>true</c> if this tag line has any flagged word units; otherwise, <c>false</c>.
/// </returns>
bool CChildesTagLine::IsFlagged()
{
	for (CChildesTagLine::Iter iter = this->begin(); iter != this->end(); ++iter)
	{
		if ((*iter)->IsFlagged())
		{
			// Just one word unit being flagged is enough to decide the whole line is flagged. 
			return true;
		}
	}
	
	return false;
}