#pragma once
#include <string>
#include <fstream>
#include <iostream>

/// <summary>
/// Reads the CHILDES File and converts it into a vector of lines.
/// </summary>
class CFileLines {

private:
	std::vector<std::wstring> mLines;

	unsigned int mPos = 0;


public:
	CFileLines(std::wifstream &file)
	{

		std::wstring line;
		while (std::getline(file, line)) {
			mLines.push_back(line);
		}
	}
	~CFileLines() {   }




	bool getline(std::wstring &line) {
		if (mPos == mLines.size() && mPos >= 0) {
			return false;
		}
		else {
			line = mLines[mPos];

			mPos++;
			return true;
		}
	}

	void seek_back() {
		if (mPos > 0) {
			mPos--;
		}
	}

	unsigned int line_number() { return mPos + 1; }



};