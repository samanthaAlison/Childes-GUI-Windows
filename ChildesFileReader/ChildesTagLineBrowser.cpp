#include "ChildesTagLineBrowser.h"
#include "ChildesTagLine.h"





/// <summary>
/// Adds the tag line.
/// </summary>
/// <param name="sections">The sections.</param>
/// <param name="parent">The parent.</param>
void CChildesTagLineBrowser::AddLine(std::map<TagLineSections::Section, std::wstring>& sections, bool processed)
{

	CChildesTagLine * tagLine = new CChildesTagLine(sections, this);

	tagLine->SetProcessed(processed);
	AddItem(tagLine);

	CChildesFile::AddLine(tagLine);

}

/// <summary>
/// Initializes a new instance of the <see cref="CChildesTagLineBrowser"/> class.
/// </summary>
/// <param name="context">The context we will be using for this file.</param>
CChildesTagLineBrowser::CChildesTagLineBrowser(CChildesReaderContext * context) : CChildesFile(context)
{
}

CChildesTagLineBrowser::~CChildesTagLineBrowser()
{
}

