#include "MultipleStatsItemInstance.h"




/// <summary>
/// Destructor.
/// </summary>
CMultipleStatsItemInstance::~CMultipleStatsItemInstance()
{
}

/// <summary>
/// Initializes a new instance of the <see cref="CMultipleStatsItemInstance"/> class.
/// </summary>
/// <param name="id">The identifier.</param>
/// <param name="manager">The manager.</param>
CMultipleStatsItemInstance::CMultipleStatsItemInstance(std::wstring id, CStatsItemManager * manager) :
	CStatsItemInstance(id, manager)
{
	
}
