/**
 * \file StatsItem.h
 * \author Samantha Oldenburg
 * 
 *
 * Describes a 
 *
 * Note that, although you can access the fields of this object like 
 * a map, IT IS NOT A MAP. Think of this more as an entry of a SQL
 * table; you must define your fields beforehand, and not add new 
 * ones dynamically. This is why the only constructors available
 * to this class are protected and require field names. 
 */
#pragma once
#include <map>
#include <vector>
#include <cassert>
class CStatsItemManager;
/// <summary>
/// A class with integer stats we can keep track of, using text identifiers for each
/// individual field.
/// </summary>
/// <remarks>
/// Note that, although you can access the fields of this object like 
/// a map, IT IS NOT A MAP.Think of this more as an entry of a SQL
/// table; you must define your fields beforehand, and not add new
/// ones dynamically.This is why the only constructors available
/// to this class are protected and require field names.
/// </remarks>
class CStatsItem
{	
	
private:
	
	/// <summary>
	/// The stats manager that owns this.
	/// </summary>
	CStatsItemManager * mManager;

public:
	/// <summary>
	///  A single field with a single numeric value.
	///
	/// We can read the value, increment it, and
	/// decrement it
	/// </summary>
	class StatsField
	{
	private:
		/// The value we are keeping track of.
		int mValue = 0;


	public:
		StatsField() = delete;
		StatsField(int v = 0) { mValue = v; }
		StatsField(const StatsField & other) { mValue = other.mValue; }

		~StatsField() {};		

		/// <summary>
		/// Increments the StatsField's value.
		/// </summary>
		/// <returns>The StatsField after incrementing the value.</returns>
		StatsField & operator++() { ++mValue; return *this; }
		
		/// <summary>
		/// Increments the StatsField's value.
		/// </summary>
		/// <returns>The StatsField before incrementing the value.</returns>
		StatsField & operator++(int) { StatsField tmp(*this); ++(*this); return tmp; }
		
		/// <summary>
		/// Decrements the StatsField's value.
		/// </summary>
		/// <returns>The StatsField after decrementing the value.</returns>
		StatsField & operator--() { --mValue; return *this; }

		/// <summary>
		/// Decrements the StatsField's value.
		/// </summary>
		/// <returns>The StatsField before decrementing the value.</returns>
		StatsField & operator--(int) { StatsField tmp(*this); --(*this); return tmp; }
				
		/// <summary>
		/// Gets the value of the StatsField.
		/// </summary>
		/// <returns>The value of the StatsField.</returns>
		int GetValue() { return mValue; }
	};
protected:
	

	CStatsItem(std::map<std::wstring, StatsField> fields);
private: 
	/// <sumary>
	/// The fields this item will be keeping track of.
	/// </sumary>
	std::map<std::wstring, StatsField> mStatsFields;
public:

	CStatsItem() = delete;
	CStatsItem(const std::vector<std::wstring> & fieldNames);
	
	/// <summary>
	/// Initializes a new instance of the <see cref="CStatsItem"/> class via 
	/// the copy constructor.
	/// </summary>
	/// <param name="item">The item we are copying.</param>
	CStatsItem(const CStatsItem &item) : CStatsItem(item.mStatsFields) {
		SetManager(item.mManager);
	}

	~CStatsItem();


	/// <summary>
	/// Sets the manager of this class.
	/// </summary>
	/// <param name="manager">The new manager.</param>
	void SetManager(CStatsItemManager * manager) { mManager = manager; }


	 /// <summary>
	 /// Get a StatsField object from this item using 
	 /// map-like syntax. Fails an assertion if
	 /// you try to use it for a field that doesn't exist
	 /// so be careful.
	 /// </summary>
	 /// <param name="fieldName">Name of the field we are looking for.</param>
	 /// <returns>The reference to the StatsField object, if it passes the assertion.</returns>
	StatsField & operator[](std::wstring fieldName)
	{
		// Make sure the StatsField exists.
		std::map<std::wstring, StatsField>::iterator iter = mStatsFields.find(fieldName);
		
		//
		// If it wasn't found, end the program immediately.
		// You obviously messed up at this point, and you're 
		// not using this class for its intended purpose. So 
		// we will stop you from breaking something even worse.
		//
		assert(iter != mStatsFields.end());

		// Okay, if you survived the assertion, we will now
		// give you the StatsField.
		
		return iter->second;

	}
};


