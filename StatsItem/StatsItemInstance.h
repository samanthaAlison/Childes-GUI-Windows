#pragma once
#include "StatsItem.h"
#include "StatsItemManager.h"
/// <summary>
/// An instance of the CStatsItem classed. Used as an intermediate class
/// in a flyweight-like way.
/// </summary>
class CStatsItemInstance
{
private:
	/// <summary>
	/// The pointer to the StatsItem object this object is an instance of.
	/// </summary>
	CStatsItem * mStatsItem;
	
	/// <summary>
	/// The  identifier of the CStatsItem this is an instance of.
	/// </summary>
	std::wstring mId;
public:
	CStatsItemInstance() = delete;
	CStatsItemInstance(std::wstring id, CStatsItemManager * manager);
	CStatsItemInstance(const CStatsItemInstance &) = delete;
	virtual ~CStatsItemInstance();
	
	/// <summary>
	/// Gets the identifier.
	/// </summary>
	/// <returns>The identifier string</returns>
	const std::wstring & GetId() { 
		return mId; 
	}
		
	/// <summary>
	/// Gets the stats item.
	/// </summary>
	/// <returns>The stats item.</returns>
	CStatsItem * GetStatsItem() { return mStatsItem; }
};

