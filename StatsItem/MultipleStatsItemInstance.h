#pragma once
#include "StatsItemInstance.h"
/// <summary>
/// A stats item usage, but used in a case where a single
/// stats item could appear multiple times in the same context,
/// and only needs to be selected in one of those instances to be
/// selected overall.
/// </summary>
/// <seealso cref="CStatsItemInstance" />
class CMultipleStatsItemInstance :
	public CStatsItemInstance
{
private:
	/// <summary>
	/// The amount of instances of this StatsItem being used.
	/// </summary>
	int mUsageCount = 0;

	/// <summary>
	/// A number that decreases when a StatsItem was not selected but increases when 
	/// it is selected. Overall, <c>mUsageCount</c> + <c>mCorrectChoiceCount</c> is the 
	/// amount of times the StatsItem was selected in this context.
	/// </summary>
	int mCorrectChoiceCount = 0;
public:
	CMultipleStatsItemInstance() = delete;
	CMultipleStatsItemInstance(const CMultipleStatsItemInstance &) = delete;
	CMultipleStatsItemInstance(std::wstring id, CStatsItemManager* manager);
	virtual ~CMultipleStatsItemInstance();


	virtual void AnalyzeStats() {}

	/// <summary>
	/// Increments the usage count. Called if a an uses
	/// this instance's CStatsItem.
	/// </summary>
	void IncrementUsageCount() 
	{ 
		mUsageCount++; 
		// Because the default form of an instance is to not be correct, 
		// automatically decrement the correct choice count.
		mCorrectChoiceCount--;
	}

	/// <summary>
	/// Decrements the correct choice count.
	/// </summary>
	void DecrementCorrectChoiceCount() { mCorrectChoiceCount--; }

	/// <summary>
	/// Increments the correct choice count.
	/// </summary>
	void IncrementCorrectChoiceCount() { mCorrectChoiceCount++; }

	/// <summary>
	/// Determines whether [this instance's CStatsItem was selected].
	/// </summary>
	/// <returns>
	///   <c>true</c> if [it is correct choice]; otherwise, <c>false</c>.
	/// </returns>
	bool IsCorrectChoice() { return ((mCorrectChoiceCount + mUsageCount) > 0); }
	
	/// <summary>
	/// Gets the amount of times this instance's CStatsItem has been used in this context.
	/// </summary>
	/// <returns>The usage count</returns>
	int GetUsageCount() { return mUsageCount; }
	
	/// <summary>
	/// Gets the amount of times this this StatsItem was selected as correct
	/// </summary>
	/// <returns></returns>
	int GetCorrectChoiceCount() { return mCorrectChoiceCount; }
};

