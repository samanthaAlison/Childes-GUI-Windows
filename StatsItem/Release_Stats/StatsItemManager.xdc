<?xml version="1.0"?><doc>
<members>
<member name="T:CStatsItemManager" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitemmanager.h" line="15">
Class that keeps track of StatsItem objects.

</member>
<member name="F:CStatsItemManager.mPrototype" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitemmanager.h" line="22">
This item is the item that all items in this
manager will be based off of. That means that
every new item this manager creates will have
the same fields as this item does. 
</member>
<member name="F:CStatsItemManager.mStatsItems" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitemmanager.h" line="28">
<summary>
The stats items.
</summary>
</member>
<member name="M:CStatsItemManager.GetItemCount" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitemmanager.h" line="41">
<summary>
Gets the StatsItem count.
</summary>
<returns>The amount of items this manager has.</returns>
</member>
<member name="M:_wassert(System.Char!System.Runtime.CompilerServices.IsConst*,System.Char!System.Runtime.CompilerServices.IsConst*,System.UInt32)" decl="true" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="1">
 \file StatsItem.h
 \author Samantha Oldenburg
 

 Describes a 

 Note that, although you can access the fields of this object like 
 a map, IT IS NOT A MAP. Think of this more as an entry of a SQL
 table; you must define your fields beforehand, and not add new 
 ones dynamically. This is why the only constructors available
 to this class are protected and require field names. 

</member>
<member name="T:CStatsItem" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="19">
<summary>
A class with integer stats we can keep track of, using text identifiers for each
individual field.
</summary>
<remarks>
Note that, although you can access the fields of this object like 
a map, IT IS NOT A MAP.Think of this more as an entry of a SQL
table; you must define your fields beforehand, and not add new
ones dynamically.This is why the only constructors available
to this class are protected and require field names.
</remarks>
</member>
<member name="F:CStatsItem.mManager" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="35">
<summary>
The stats manager that owns this.
</summary>
</member>
<member name="T:CStatsItem.StatsField" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="41">
<summary>
 A single field with a single numeric value.

We can read the value, increment it, and
decrement it
</summary>
</member>
<member name="F:CStatsItem.StatsField.mValue" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="50">
The value we are keeping track of.
</member>
<member name="M:CStatsItem.StatsField.op_Increment" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="61">
<summary>
Increments the StatsField's value.
</summary>
<returns>The StatsField after incrementing the value.</returns>
</member>
<member name="M:CStatsItem.StatsField.op_Increment(System.Int32)" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="67">
<summary>
Increments the StatsField's value.
</summary>
<returns>The StatsField before incrementing the value.</returns>
</member>
<member name="M:CStatsItem.StatsField.op_Decrement" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="73">
<summary>
Decrements the StatsField's value.
</summary>
<returns>The StatsField after decrementing the value.</returns>
</member>
<member name="M:CStatsItem.StatsField.op_Decrement(System.Int32)" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="79">
<summary>
Decrements the StatsField's value.
</summary>
<returns>The StatsField before decrementing the value.</returns>
</member>
<member name="M:CStatsItem.StatsField.GetValue" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="85">
<summary>
Gets the value of the StatsField.
</summary>
<returns>The value of the StatsField.</returns>
</member>
<member name="F:CStatsItem.mStatsFields" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="96">
<sumary>
The fields this item will be keeping track of.
</sumary>
</member>
<member name="M:CStatsItem.#ctor(CStatsItem!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="105">
<summary>
Initializes a new instance of the <see cref="T:CStatsItem"/> class via 
the copy constructor.
</summary>
<param name="item">The item we are copying.</param>
</member>
<member name="M:CStatsItem.SetManager(CStatsItemManager*)" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="117">
<summary>
Sets the manager of this class.
</summary>
<param name="manager">The new manager.</param>
</member>
<member name="M:CStatsItem.op_Subscript(std.basic_string&lt;System.Char,std.char_traits{System.Char},std.allocator&lt;System.Char&gt;&gt;)" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitem.h" line="124">
<summary>
Get a StatsField object from this item using 
map-like syntax. Fails an assertion if
you try to use it for a field that doesn't exist
so be careful.
</summary>
<param name="fieldName">Name of the field we are looking for.</param>
<returns>The reference to the StatsField object, if it passes the assertion.</returns>
</member>
<member name="M:CStatsItemManager.AddStatsItem(std.basic_string&lt;System.Char,std.char_traits{System.Char},std.allocator&lt;System.Char&gt;&gt;)" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitemmanager.cpp" line="7">
<summary>
Create a new CStatsItem, add it to the mStatsItem map,
and then set this as that CStatsItem's manager.
This is used when we try to access a CStatsItem at
an identifier that doesn't exist yet.
</summary>
<param name="id">The identifier to be associated with the CStatsItem</param>
<returns>
The CStatsItem we just created
</returns>
</member>
<member name="M:CStatsItemManager.#ctor(std.vector&lt;std.basic_string&lt;System.Char,std.char_traits{System.Char},std.allocator&lt;System.Char&gt;&gt;,std.allocator&lt;std.basic_string&lt;System.Char,std.char_traits{System.Char},std.allocator&lt;System.Char&gt;&gt;&gt;&gt;)" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitemmanager.cpp" line="31">
<summary>
Initializes a new instance of the <see cref="T:CStatsItemManager"/> class.
</summary>
<param name="fieldNames">The field names of the prototype item.</param>
</member>
<member name="M:CStatsItemManager.Dispose" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitemmanager.cpp" line="41">
<summary>
Destructor of the <see cref="T:CStatsItemManager"/> class.
Deletes all of the StatsItems here, so make sure not to
delete them anywhere else.
</summary>
</member>
<member name="M:CStatsItemManager.ItemAt(std.basic_string&lt;System.Char,std.char_traits{System.Char},std.allocator&lt;System.Char&gt;&gt;)" decl="false" source="c:\users\samantha\source\repos\childes-gui\statsitem\statsitemmanager.cpp" line="58">
<summary>
Get the CStatsItem with the specified id.
</summary>
<param name="id">The identifier.</param>
<returns>The item. If the id does not exist yet, and CStatsItem will be created
and associated with that id</returns>
</member>
</members>
</doc>