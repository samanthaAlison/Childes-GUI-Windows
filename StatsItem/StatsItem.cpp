#include "StatsItem.h"
#include "StatsItemManager.h"
/// <summary>
/// Initializes a new instance of the <see cref="CStatsItem"/> class.
/// </summary>
/// <param name="fieldNames">Contains the field names and the values of the fields</param>
CStatsItem::CStatsItem(std::map<std::wstring, StatsField> fields)
{
	mStatsFields.insert(fields.begin(), fields.end());
}


 /// <summary>
 /// Initializes a new instance of the <see cref="CStatsItem"/> class.
 /// </summary>
 /// <param name="fieldNames">The field names.</param>
CStatsItem::CStatsItem(const std::vector<std::wstring> &fieldNames)
{
	for (std::wstring field : fieldNames)
	{
		mStatsFields.emplace(std::make_pair(field, StatsField(0)));
	}
}

/// <summary>
/// Destructor of the <see cref="CStatsItem"/> class.
/// </summary>
CStatsItem::~CStatsItem()
{
}
