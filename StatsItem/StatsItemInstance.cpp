#include "StatsItemInstance.h"






/// <summary>
/// Initializes a new instance of the <see cref="CStatsItemInstance"/> class.
/// </summary>
/// <param name="id">The identifier of the CStatsItem we are getting.</param>
/// <param name="manager">The manager we are getting the item from.</param>
CStatsItemInstance::CStatsItemInstance(std::wstring id, CStatsItemManager * manager)
{
	mId = std::wstring(id);

	mStatsItem = manager->ItemAt(id);
}

/// <summary>
/// Destructor of the <see cref="CStatsItemInstance"/> class.
/// </summary>
CStatsItemInstance::~CStatsItemInstance()
{
}
