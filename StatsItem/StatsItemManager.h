/**
 * \file StatsItemManager.h
 *
 * \author Samantha Oldenburg
 *
 * Describes a class that keeps track of objects of a single 
 * type derived from the StatsItem class.
 *
 */

#pragma once
#include <string>
#include <map>
#include <vector>
class CStatsItem;
/**
 * Class that keeps track of StatsItem objects.
 */

class CStatsItemManager
{
private:
	/// This item is the item that all items in this
	/// manager will be based off of. That means that
	/// every new item this manager creates will have
	/// the same fields as this item does. 
	CStatsItem * mPrototype;

	/// <summary>
	/// The stats items.
	/// </summary>
	std::map<std::wstring, CStatsItem *> mStatsItems;


	CStatsItem * AddStatsItem(std::wstring id);
public:
	CStatsItemManager()=delete;
	CStatsItemManager(std::vector<std::wstring> fieldNames);
	virtual ~CStatsItemManager();
	CStatsItem * ItemAt(std::wstring id);
	
	/// <summary>
	/// Gets the StatsItem count.
	/// </summary>
	/// <returns>The amount of items this manager has.</returns>
	unsigned int GetItemCount() { return (int) mStatsItems.size(); }
};

