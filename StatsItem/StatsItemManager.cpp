#include "StatsItemManager.h"
#include "StatsItem.h"
#include <iterator>
#include <vector>
#include <algorithm>

/// <summary>
/// Create a new CStatsItem, add it to the mStatsItem map,
/// and then set this as that CStatsItem's manager.
/// This is used when we try to access a CStatsItem at
/// an identifier that doesn't exist yet.
/// </summary>
/// <param name="id">The identifier to be associated with the CStatsItem</param>
/// <returns>
/// The CStatsItem we just created
/// </returns>
CStatsItem * CStatsItemManager::AddStatsItem(std::wstring id)
{
	CStatsItem * item = new CStatsItem(*mPrototype);
	std::map<std::wstring, CStatsItem *>::iterator iter = mStatsItems.find(id);	
	// Make sure to delete any previous stat item. 
	if (iter != mStatsItems.end())
	{
		delete iter->second;
	}
	mStatsItems.insert(std::make_pair(id, item));

	return item;
}

/// <summary>
/// Initializes a new instance of the <see cref="CStatsItemManager"/> class.
/// </summary>
/// <param name="fieldNames">The field names of the prototype item.</param>
CStatsItemManager::CStatsItemManager(std::vector<std::wstring> fieldNames)
{
	mPrototype = new CStatsItem(fieldNames);
	mPrototype->SetManager(this);
}

/// <summary>
/// Destructor of the <see cref="CStatsItemManager"/> class.
/// Deletes all of the StatsItems here, so make sure not to
/// delete them anywhere else.
/// </summary>
CStatsItemManager::~CStatsItemManager()
{
	// Delete all the CStatsItem objects.
	delete mPrototype;
	for (std::map<std::wstring, CStatsItem *>::iterator iter = mStatsItems.begin();
		iter != mStatsItems.end();
		++iter)
	{
		delete iter->second;
	}
}

/// <summary>
/// Get the CStatsItem with the specified id.
/// </summary>
/// <param name="id">The identifier.</param>
/// <returns>The item. If the id does not exist yet, and CStatsItem will be created
/// and associated with that id</returns>
CStatsItem * CStatsItemManager::ItemAt(std::wstring id)
{
	std::map<std::wstring, CStatsItem *>::iterator iter = mStatsItems.find(id);
	if (iter != mStatsItems.end())
	{
		return iter->second;
	}
	else
	{
		return AddStatsItem(id);
	}
}
